\documentclass[12pt, letter]{report}

\usepackage{amsmath, mathrsfs, amssymb}    % need for subequations
\usepackage[pdftex]{graphicx}   % need for figures
\usepackage{verbatim}   % useful for program listings
\usepackage{color}      % use if color is used in text
\usepackage{hyperref}   % use for hypertext links, including those to external documents and URLs
\usepackage{breqn}
\allowdisplaybreaks

\graphicspath{{figs/}}

\begin{document}
The axisymmetric velocity of the jet is defined as $\textbf{u}=(u,v,0)$. The velocities can be expressed in terms of the Stoke's stream function.
\begin{equation}
u = \frac{1}{r} \frac{\partial \Psi}{\partial r} \quad v = - \frac{1}{r} \frac{\partial \Psi}{\partial x}
\end{equation}
The incompressible Euler equation in stream function form can be written as.
\begin{equation}
\frac{\partial }{\partial t} G \Psi + \frac{1}{r}\left( D_r \Psi D_x - D_x \Psi H\right) G \Psi = 0
\end{equation}
The boundary conditions are
\begin{equation}
\begin{split}
D_r \Psi(r,0,t) = u_0(t) r P(r)  \quad & D_r D_x \Psi(r,1,t)=0 \quad D_x \Psi(r_0, x, t)=0 \\
& \text{u(0,x,t), v(0,x,t) finite} 
\end{split} 
\end{equation}
where $G= D^2 - \frac{D}{r} + \frac{\partial^2 }{\partial x^2}$, $H=D - \frac{2}{r}$, $P(r)=\frac{1}{2}\left( 1 - \tanh \left( \frac{1-r}{2 \theta} \right) \right)$ is the jet profile. 

Make the substitution \begin{equation}
\begin{split}
\Psi(r,x,t) &= \psi(r,x,t)+u_0(t) \chi_0(r,x) \\
\chi_0(r,x) &= (x-1)^2 \int r P(r) dr
\end{split}
\end{equation}
This substitution removes the inhomogeneity from the boundary conditions and introduces one into the equation of motion itself.
\begin{equation}
\begin{split}
& \left( D_t + \frac{1}{r} ( D_r u_0(t) \chi_0 D_x - D_x u_0(t) \chi_0 H) \right) G \psi \\
&+ \frac{1}{r}(D_x G u_0(t) \chi_0 D_r - H G u_0(t) \chi_0 D_x ) \psi \\ 
&+ \frac{1}{r}\left( D_r \psi D_x G \psi - D_x \psi H G \psi\right) \\
&+ \frac{1}{r}\left( D_r u_0(t) \chi_0 D_x G u_0(t) \chi_0 - D_x u_0(t) \chi_0 H G u_0(t) \chi_0 \right) \\ 
& + \dot{u}_0(t) G\chi_0 = 0 
\end{split}
\end{equation}
The boundary conditions become.
\begin{equation}
\begin{split}
& D_r \psi(r,0,t) = 0 \quad D_r D_x \psi(r,1,t) =0 \quad D_x \psi(r_0, x, t) =0 \\
& \frac{1}{r}D_x \psi(0, x, t) \quad \text{and} \quad \frac{1}{r}D_r \psi(0, x, t) \quad \text{finite} 
\end{split} 
\end{equation}
Solve BVP
\begin{equation}
\begin{split}
G \psi_{ij}(r,x) &= \omega_{ij} \psi_{ij}(r,x) \\
\psi_{ij}(r,0,t) &= 0 \quad \frac{\partial \psi_{ij}(r,1,t)}{\partial x}=0 \\ 
\psi_{ij}(0, x, t) & \quad \text{is finite} \quad \psi_{ij}(r_0, x, t)=0 
\end{split}
\end{equation}
Solutions are
\begin{equation}
\begin{split}
\psi_{ij}(r,x) &= r J_1 (j_{1,i} \frac{r}{r_0}) \sin \left( \left(j-\frac{1}{2} \right) \pi x\right) \\ \omega_{ij} &= \left( \frac{j_{1,i}}{r_0} \right)^2 + (2j-1)^2 \frac{\pi^2}{4} \quad i,j \geq 1
\end{split}
\end{equation}
If the number of modes are truncated to $N_x$ axial modes and $N_r$ radial modes. The subscripts $i$ and $j$ can be expressed as a function of a single subscript $n$, such that
\begin{equation}
\begin{split}
\psi_{n}(r,x) &= r J_1 (j_{1,i(n)} \frac{r}{r_0}) \sin \left( \left(j(n)-\frac{1}{2} \right) \pi x\right) \\ \omega_{n} &= \left( \frac{j_{1,i(n)}}{r_0} \right)^2 + (2j(n)-1)^2 \frac{\pi^2}{4}\\
i(n) &= floor(n/N_r) + 1, \quad j(n) = n - i(n) N_r + 1, \quad n \geq 0
\end{split}
\end{equation}
Orthogonality relation is
\begin{multline}
<\psi_{n}(r,x),\psi_{m}(r,x)>=\int_0^{r_0} \int_0^1 \frac{1}{r}\psi_{n}(r,x) \psi_{m}(r,x) dr dx = \Lambda_{n}\delta_{nm}  
\end{multline}

Now revisit equation with inhomogeneous driving term.
\begin{equation}
\begin{split}
& D_t G \psi + \frac{u_0(t)}{r} ( D_r \chi_0 D_x G - D_x \chi_0 H G + D_x G \chi_0 D_r - H G \chi_0 D_x ) \psi \\ 
& +\frac{1}{r}\left( D_r \psi D_x G \psi - D_x \psi H G \psi\right) \\
& +\frac{ u_0(t)^2}{r}\left( D_r \chi_0 D_x G \chi_0 - D_x \chi_0 H G \chi_0 \right) + \dot{u}_0(t) G\chi_0 = 0
\end{split}
\end{equation}
Assuming $\psi(r,x,t)=\sum_{m} a_{m}(t)\psi_{m}(r,x)$.
\begin{equation}
\begin{split}
& \sum_{m} \omega_{m}\dot{a}_{m}(t)\psi_{m} \\
& + \frac{u_0(t)}{r} \sum_{m} a_{m}(t) ( D_r \chi_0 D_x G - D_x \chi_0 H G + D_x G \chi_0 D_r - H G \chi_0 D_x ) \psi_{m} \\ 
& +\frac{1}{r} \sum_{m,l} a_{m}(t)a_{l}(t)( D_r \psi_{m} D_x G \psi_{l} - D_x \psi_{m} H G \psi_{l}) \\ 
& +  \frac{ u_0(t)^2}{r}\left( D_r \chi_0 D_x G \chi_0 - D_x \chi_0 H G \chi_0 \right) + \dot{u}_0(t) G \chi_0 =0
\end{split}
\end{equation}
Taking the inner product of this equation with $\psi_{n}$ 
\begin{equation}
\label{eq:stream_modes}
\dot{a}_{n}(t) + u_0(t) \sum_{m=0}^{N_a} B_{mn} a_{m}(t) + \sum_{l,m=0}^{N_a}
C_{lmn} a_{m}(t)a_{l}(t) + u_0(t)^2 d_{n} + \dot{u}_0(t) f_{n}= 0
\end{equation}
\begin{equation}
\begin{split}
\Lambda_{n} &= <\psi_{n}, \psi_{n}> \\
\omega_{n} \Lambda_{n}B_{mn} &= <\psi_{n},\frac{1}{r}( \omega_{m} D_r \chi_0 D_x  - \omega_{m} D_x \chi_0 H + D_x G \chi_0 D_r - H G \chi_0 D_x ) \psi_{m}> \\
\omega_{n} \Lambda_{n} C_{lmn} &= <\psi_{n}, \frac{\omega_{l} }{r}(D_r \psi_{m} D_x \psi_{l} - D_x \psi_{m} H \psi_{l})> \\
\omega_{n} \Lambda_{n} d_{n} &= <\psi_{n}, \frac{1}{r}(D_r \chi_0 D_x G \chi_0 - D_x \chi_0 H G \chi_0 )> \\
\omega_{n} \Lambda_{n} f_{n} &= <\psi_{n}, G \chi_0>
\end{split}
\end{equation}
\section{Connection to Acoustic Flow}
\begin{equation}
\label{eq:u_dot}
\dot{u}_f = \mu^{-1} \left(p - p_{src} + \sum_{i}^{N_q} \dot{q}_i -\frac{\gamma}{2} u_{f}^2  \right)
\end{equation}
\begin{equation}
\label{eq:acoustic}
\vec{\ddot{q}} + \boldsymbol{D} \vec{\dot{q}} + \frac{\gamma }{\mu^2} \frac{u_f}{C_E} \vec{\beta} \cdot \vec{\dot{q}} + \boldsymbol{K} \vec{q} =\vec{\beta} \frac{\gamma}{\mu} \dot{p}_{src} -\vec{\beta} \frac{\gamma }{\mu^2} \frac{u_f}{C_E} p_{src}  -\left(\vec{\alpha} + \vec{\beta}  \frac{\gamma }{\mu^2}p \right) \frac{u_f}{C_E} + \vec{\beta}  \frac{\gamma^2 }{2\mu^2} u_{0}^3,
\end{equation} 
The pressure source due to vorticity is given by
\begin{equation}
\begin{split}
p_{src} &= \sum_i \zeta_i \dot{a}_i \\
\zeta_i &= -\frac{1}{r_0^2}\int_0^{r_0}\int_0^1 \nabla^2 \psi_i(r,x) dr dx
\end{split}
\end{equation}
Substituting Eq. \ref{eq:stream_modes},
\begin{equation}
p_{src} = -u_0 \sum_{m,n=0}^{N_a} B_{mn} \zeta_n a_{m} - \sum_{l,m,n=0}^{N_a}
C_{lmn} \zeta_n a_{m} a_{l} - u_0^2 \sum_{n=0}^{N_a} d_{n} \zeta_n - \dot{u}_0 \sum_{n=0}^{N_a} f_{n} \zeta_n
\end{equation}
The substituting in Eq. \ref{eq:u_dot} for the $\dot{u}_0$ term and solving for $p_{src}$.
\begin{multline}
\left( 1 + \mu^{-1} \right)p_{src} = -u_0 \sum_{m,n=0}^{N_a} B_{mn} \zeta_n a_{m} - \sum_{l,m,n=0}^{N_a}
C_{lmn} \zeta_n a_{m} a_{l} - u_0^2 \sum_{n=0}^{N_a} \left(d_{n} \zeta_n - \frac{\gamma}{2\mu} \sum_{n=0}^{N_a} f_{n}\zeta_n \right) - \\ \mu^{-1} \left(p - \sum_{i}^{N_q} \dot{q}_i \right) \sum_{n=0}^{N_a} f_{n} \zeta_n
\end{multline}
Writing this in vector form
\begin{multline}
\label{eq:p_{src}}
p_{src} = -\frac{u_0}{1 + \mu^{-1}} \textbf{B} . \vec{a} . \vec{\zeta}  - \frac{u_0^2}{1 + \mu^{-1} } \left(\vec{d}-\frac{\gamma}{2\mu}\vec{f} \right) . \vec{\zeta}  - \frac{\vec{f} . \vec{\zeta}}{1+\mu} \left(p - \sum_{i}^{N_q} \dot{q}_i \right) - \textbf{C} . (\vec{a} \otimes \vec{a}) . \vec{\zeta}
\end{multline}
Now taking the time derivative of this expression
\begin{multline}
\dot{p}_{src} = -\frac{1}{1+\mu^{-1}}\dot{u}_0 \textbf{B} . \vec{a} . \vec{\zeta}  - \frac{1}{1+\mu^{-1}}u_0 \textbf{B} . \vec{\dot{a}} . \vec{\zeta}  - \frac{2}{1+\mu^{-1}} u_0 \dot{u}_0 \left(\vec{d}-\frac{\gamma}{2\mu}\vec{f} \right) . \vec{\zeta} + \frac{\vec{f} . \vec{\zeta}}{1+\mu}\sum_{i}^{N_q} \ddot{q}_i \\ - \frac{1}{1+\mu^{-1}} \textbf{C} . (\vec{\dot{a}} \otimes \vec{a} + \vec{a} \otimes \vec{\dot{a}}) . \vec{\zeta}=F(u_0, \dot{u}_0, \vec{a}, \vec{\dot{a}}, r_f, p) + \frac{\vec{f} . \vec{\zeta}}{1+\mu}\sum_{i}^{N_q} \ddot{q}_i
\end{multline}
Substituting these expressions into Eq. \ref{eq:acoustic}
\begin{equation}
\label{eq:acoustic}
\textbf{R}^-1 \vec{\ddot{q}}+ \left(\boldsymbol{D} + \vec{\beta} \frac{\gamma }{\mu^2} u_0 \right) \vec{\dot{q}} + \boldsymbol{K} \vec{q} =\vec{\beta} \frac{\gamma}{\mu} F(u_0, \dot{u}_0, \vec{a}, \vec{\dot{a}}, r_f, p) -\vec{\beta} \frac{\gamma }{\mu^2} u_0 p_{src}  -\left(\vec{\alpha} + \vec{\beta}  \frac{\gamma }{\mu^2}p \right) u_0 + \vec{\beta}  \frac{\gamma^2 }{2\mu^2} u_{0}^3,
\end{equation} 
where
\begin{equation}
R^{-1}_{ij} = \delta_{ij} -\vec{\beta} \frac{\gamma \vec{f} . \vec{\zeta}}{\mu(1+\mu)}
\end{equation}
This can be solved by 



\begin{multline}
u_0 \dot{u}_0 = \frac{p u_0}{\mu} -\frac{u_0^2}{1 + \mu} \textbf{B} . \vec{a} . \vec{\zeta}  - \frac{u_0^3}{1 + \mu} \left(\vec{d}-\frac{\gamma}{2\mu}\vec{f} \right) . \vec{\zeta}  - \frac{\vec{f} . \vec{\zeta} u_0}{\mu(1+\mu)} \left(p - \sum_{i}^{N_q} \dot{q}_i \right) \\ 
- \frac{u_0}{\mu}\textbf{C} . (\vec{a} \otimes \vec{a}) . \vec{\zeta} - \frac{u_0}{\mu}\sum_{i}^{N_q} \dot{q}_i -\frac{\gamma}{2 \mu} u_{0}^3
\end{multline} 
\begin{multline}
u_0 \dot{u}_0 = \left( \frac{1}{\mu} - \frac{\vec{f} . \vec{\zeta}}{\mu(1+\mu)} \right) p u_0 - \left( \frac{1}{\mu} - \frac{\vec{f} . \vec{\zeta}}{\mu(1+\mu)} \right) u_0 \sum_{i}^{N_q} \dot{q}_i \\ -\frac{u_0^2}{1 + \mu} \textbf{B} . \vec{a} . \vec{\zeta}  - \left(\frac{\left(\vec{d}-\frac{\gamma}{2\mu}\vec{f} \right). \vec{\zeta}}{1 + \mu} + \frac{\gamma}{2 \mu} \right) u_0^3  
- \frac{u_0}{\mu}\textbf{C} . (\vec{a} \otimes \vec{a}) . \vec{\zeta} 
\end{multline} 
Substituting into expression for $p_{src}$
\begin{multline}
\dot{p}_{src} = -\frac{1}{1+\mu} \left(p + p_{src} - \sum_{i}^{N_q} \dot{q}_i -\frac{\gamma}{2} u_{0}^2  \right) \textbf{B} . \vec{a} . \vec{\zeta}  - \frac{u_0}{1+\mu^{-1}} \textbf{B} . \vec{\dot{a}} . \vec{\zeta}  - \\ 2\left(\vec{d}-\frac{\gamma}{2\mu}\vec{f} \right) . \vec{\zeta} (\left( \frac{1}{1+\mu} - \frac{\vec{f} . \vec{\zeta}}{(1+\mu)^2} \right) p u_0 - \left( \frac{1}{1+\mu} - \frac{\vec{f} . \vec{\zeta}}{(1+\mu)^2} \right) u_0 \sum_{i}^{N_q} \dot{q}_i  -\frac{\mu u_0^2}{(1 + \mu)^2} \textbf{B} . \vec{a} . \vec{\zeta}  - \\  \left(\frac{\mu \left(\vec{d}-\frac{\gamma}{2\mu}\vec{f} \right). \vec{\zeta}}{(1 + \mu)^2} + \frac{\gamma}{2 (1+\mu)} \right) u_0^3  
- \frac{u_0}{1+\mu}\textbf{C} . (\vec{a} \otimes \vec{a}) . \vec{\zeta} ) \\ + \frac{\vec{f} . \vec{\zeta}}{1+\mu}\sum_{i}^{N_q} \ddot{q}_i - \frac{1}{1+\mu^{-1}}\textbf{C} . (\vec{\dot{a}} \otimes \vec{a} + \vec{a} \otimes \vec{\dot{a}}) . \vec{\zeta}
\end{multline}
\begin{multline}
\dot{p}_{src} = -\frac{1}{1+\mu} (p + \\ (-\frac{u_0}{1 + \mu^{-1}} \textbf{B} . \vec{a} . \vec{\zeta}  - \frac{u_0^2}{1 + \mu^{-1} } \left(\vec{d}-\frac{\gamma}{2\mu}\vec{f} \right) . \vec{\zeta}  - \frac{\vec{f} . \vec{\zeta}}{1+\mu} \left(p - \sum_{i}^{N_q} \dot{q}_i \right) - \textbf{C} . (\vec{a} \otimes \vec{a}) . \vec{\zeta})- \sum_{i}^{N_q} \dot{q}_i -\frac{\gamma}{2} u_{0}^2 ) \textbf{B} . \vec{a} . \vec{\zeta} \\  - \frac{u_0}{1+\mu^{-1}} \textbf{B} . \vec{\dot{a}} . \vec{\zeta}  - \\ 2\left(\vec{d}-\frac{\gamma}{2\mu}\vec{f} \right) . \vec{\zeta} (\left( \frac{1}{1+\mu} - \frac{\vec{f} . \vec{\zeta}}{(1+\mu)^2} \right) p u_0 - \left( \frac{1}{1+\mu} - \frac{\vec{f} . \vec{\zeta}}{(1+\mu)^2} \right) u_0 \sum_{i}^{N_q} \dot{q}_i  -\frac{\mu u_0^2}{(1 + \mu)^2} \textbf{B} . \vec{a} . \vec{\zeta}  - \\  \left(\frac{\mu \left(\vec{d}-\frac{\gamma}{2\mu}\vec{f} \right). \vec{\zeta}}{(1 + \mu)^2} + \frac{\gamma}{2 (1+\mu)} \right) u_0^3  
- \frac{u_0}{1+\mu}\textbf{C} . (\vec{a} \otimes \vec{a}) . \vec{\zeta} ) \\ + \frac{\vec{f} . \vec{\zeta}}{1+\mu}\sum_{i}^{N_q} \ddot{q}_i - \frac{1}{1+\mu^{-1}}\textbf{C} . (\vec{\dot{a}} \otimes \vec{a} + \vec{a} \otimes \vec{\dot{a}}) . \vec{\zeta}
\end{multline}
\end{document}
