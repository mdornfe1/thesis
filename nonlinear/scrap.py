from PyDSTool import *
from pylab import *
from itertools import chain

L = 0.004
uc = 343
r0 = 0.003 / L
dx = 0.01
dr = 0.01
x = np.arange(0, 1+dx, dx)
r = np.arange(1e-20, r0+dr, dr)

#martrix.shape=(x,r)
def inner(f1, f2):
	return np.sum(f1 * f2 * dx * dr / r )

def z1_to_z2(n, Nr):
	""" 
	Converts the index of a 1d array to a 2d array with Nr columns.
	These indices start 1.
	"""
	
	i = int(floor(n/Nr))
	j = n - i * Nr

	return i, j

def calc_psi(n, Nr):
	i, j = z1_to_z2(n, Nr)
	i += 1
	j += 1
	j1j = jn_zeros(1, j)[-1]
	R = r * j1(j1j * r / r0)
	X = np.sin((i-0.5)*pi*x)

	return np.outer(X, R)

def calc_psi_dr(n, Nr):
	i, j = z1_to_z2(n, Nr)
	i += 1
	j += 1
	j1j = jn_zeros(1, j)[-1]
	R = (j1(j1j * r / r0) - j1j * r / 2 / r0 * 
		( j1(j1j * r / r0) - jn(2, j1j * r / r0) ) )
	X = np.sin((i-0.5)*pi*x)

	return np.outer(X, R)

def calc_omega(n, Nr):
	i, j = z1_to_z2(n, Nr)
	i += 1
	j += 1
	j1j = jn_zeros(1, j)[-1]
	return j1j **2 / r0**2 + (2*i-1)**2 * pi**2 / 4

def calc_chi(rf, beta=1.625):
	delta = rf / 10
	#b = 0.25 * rf / delta
	P = 0.5 * (1 + np.tanh(beta * (rf/r - r/rf)))
	R = np.cumsum(r * P * dr)
	X = (x-1)**2
	chi = np.outer(X, R)
	chi_dr = np.outer(X, r * P)
	chi_G = Dr(chi_dr) - chi_dr / r + Dx(Dx(chi))
	chi_H = chi_dr + chi / r

	return chi, chi_dr, chi_G, chi_H  

def Dr(f):
	return np.gradient(f)[1] / dr

def Dx(f):
	return np.gradient(f)[0] / dx

def G(f):
	return Dr(Dr(f)) - Dr(f) / r + Dx(Dx(f))

def H(f):
	return Dr(f) - 2 * f / r

def calc_Lambda(n, Nr):
	psi_n = calc_psi(n, Nr)

	return inner(psi_n, psi_n)

def calc_b(rf, beta, m, n, Nr):
	chi, chi_dr, chi_G, chi_H = calc_chi(rf, beta)
	psi_m = calc_psi(m, Nr)
	psi_n = calc_psi(n, Nr)
	omega_m = calc_omega(m, Nr)
	omega_n = calc_omega(n, Nr)
	Lambda_n = calc_Lambda(n, Nr)
	right = (omega_m * chi_dr * Dx(psi_m) - omega_m * Dx(chi) * H(psi_m) + 
		Dx(chi_G) * Dr(psi_m) - H(chi_G) * Dx(psi_m) ) / r

	return inner(psi_n, right) / omega_n / Lambda_n

def calc_c(rf, beta, l, m, n, Nr):
	chi, chi_dr, chi_G, chi_H = calc_chi(rf, beta)
	psi_l = calc_psi(l, Nr)
	psi_m = calc_psi(m, Nr)
	psi_n = calc_psi(n, Nr)
	omega_l = calc_omega(l, Nr)
	omega_n = calc_omega(n, Nr)
	Lambda_n = calc_Lambda(n, Nr)
	right = omega_l / r * (Dr(psi_m) * Dx(psi_l) - Dx(psi_m) * H(psi_l) )

	return inner(psi_n, right) / omega_n / Lambda_n

def calc_d(rf, beta, n, Nr):
	chi, chi_dr, chi_G, chi_H = calc_chi(rf, beta)
	psi_n = calc_psi(n, Nr)
	omega_n = calc_omega(n, Nr)
	Lambda_n = calc_Lambda(n, Nr)
	right = (chi_dr * Dx(chi_G) - Dx(chi) * H(chi_G)) / r

	return inner(psi_n, right) / omega_n / Lambda_n

def calc_bcd(rf, beta, N, Nr):
	b = np.zeros((N, N))
	c = np.zeros((N, N, N))
	d = np.zeros(N)

	for n in range(N):
		d[n] = calc_d(rf, beta, n, Nr)
		for m in range(N):
			b[m,n] = calc_b(rf, beta, m, n, Nr)
			for l in range(N):
				c[l,m,n] = calc_c(rf, beta, l, m, n, Nr)

	return b, c, d

def sym_sum(symbol_list):
	"""For some reason summing lists of symbolic variables with 
	default sum function throws and error. Use this instead.
	"""

	summed_list = symbol_list[0]
	for symbol in symbol_list[1:]:
		summed_list += symbol

	return summed_list

def flatten(list_of_lists, n=1):
	"""Flattens list_of_lists to level n"""

	n = n-1
	flattened_list = list(chain.from_iterable(list_of_lists))
	if n == 0:
		return flattened_list
	else:
		flattened_list = flatten(flattened_list, n)

	return flattened_list

if __name__ == '__main__':
	rf = 0.0009 / L
	beta = 10
	Nr = 2
	Nx = 4
	N = Nx * Nr

	#setup paramters 
	b, c, d = ti.calc_bcd(rf, beta, N, Nr)
	u0 = Par(0.1, 'u0')
	b_sym = ([[Par(b[n,m], 'b' + str(n) + str(m)) for n in range(N)] 
		for m in range(N)] )
	c_sym = ([[[Par(c[n,m,l], 'c' + str(n) + str(m) + str(l)) 
		for n in range(N)] for m in range(N)] for l in range(N)] )
	d_sym = [Par(d[n], 'd' + str(n)) for n in range(N)]

	#setup dependent variables and equations
	a_sym = [Var('a' + str(n)) for n in range(N)]
	linear = ([sym_sum([b_sym[n][m] * a_sym[m] for m in range(N)]) 
		for n in range(N)])
	nonlinear = ([sym_sum([c_sym[n][m][l]*a_sym[m]*a_sym[l] for m in range(N) 
		for l in range(N)]) for n in range(N)])
	driving = [d_sym[n] for n in range(N)]
	eqns_rhs = ([-u0 * linear[n] - nonlinear[n] - u0**2 * driving[n] 
		for n in range(N)] )

	#setup ode system
	var_specs_dict = {'a' + str(n): eqns_rhs[n] for n in range(N)}
	ics_dict = {'a' + str(n): 0 for n in range(N)}
	DSargs = args(name='stream')
	DSargs.pars = [u0] + flatten(b_sym) + flatten(c_sym, n=2) + d_sym
	DSargs.varspecs = args(**var_specs_dict)
	DSargs.fnspecs = args(**{'f':('[x]', x*x) }
	DSargs.ics = args(**ics_dict)
	DSargs.tdomain = [0,1000] 
	ode = Generator.Vode_ODEsystem(DSargs)

	# integrate ODE
	traj = ode.compute('stream')              
	pts  = traj.sample(dt=0.1)

	plot(pts['a0'])
	plot(pts['a1'])



