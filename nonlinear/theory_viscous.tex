\documentclass[12pt, letter]{report}

\usepackage{amsmath, mathrsfs, amssymb}    % need for subequations
\usepackage[pdftex]{graphicx}   % need for figures
\usepackage{verbatim}   % useful for program listings
\usepackage{color}      % use if color is used in text
\usepackage{hyperref}   % use for hypertext links, including those to external documents and URLs
\allowdisplaybreaks

\graphicspath{{figs/}}

\begin{document}
The axisymmetric velocity of the jet is defined as $\textbf{u}=(u,v,0)$. The velocities can be expressed in terms of the Stoke's stream function.
\begin{equation}
u = \frac{1}{r} \frac{\partial \Psi}{\partial r} \quad v = - \frac{1}{r} \frac{\partial \Psi}{\partial x}
\end{equation}
The incompressible Navier-Stokes equation in stream function form can be written as.
\begin{equation}
\left(\frac{\partial }{\partial t} G \Psi - \frac{G}{R}  \right) G \Psi  + \frac{1}{r}\left( D_r \Psi D_x - D_x \Psi H\right) G \Psi = 0
\end{equation}
The boundary conditions are
\begin{equation}
\begin{split}
D_r \Psi(r,0,t) = u_0(t) r P(r)  \quad & D_r D_x \Psi(r,1,t)=0 \quad D_x \Psi(r_0, x, t)=0 \\
& \text{u(0,r,t), v(0,r,t) finite} 
\end{split} 
\end{equation}
where $G= D^2 - \frac{D}{r} + \frac{\partial^2 }{\partial x^2}$, $H=D - \frac{2}{r}$, $P(r)=\frac{1}{2}\left( 1 - \tanh \left( \frac{1-r}{2 \theta} \right) \right)$ is the jet profile. 

Make the substitution $\Psi(r,x,t)=\psi(r,x,t)+(x-1)^2 u_0(t) \int r P(r) dr=\psi(r,x,t)+u_0(t) \chi_0(r,x)$, where $\chi_0(r,x)=(x-1)^2 \int r P(r) dr$
\begin{equation}
\begin{split}
\Psi(r,x,t) &= \psi(r,x,t)+u_0(t) \chi_0(r,x) \\
\chi_0(r,x) &= (x-1)^2 \int r P(r) dr
\end{split}
\end{equation}
This substitution removes the inhomogeneity from the boundary conditions and introduces one into the equation of motion itself.
\begin{equation}
\begin{split}
& \left( D_t + \frac{1}{r} ( D_r u_0(t) \chi_0 D_x - D_x u_0(t) \chi_0 H) - \frac{G}{R} \right) G \psi \\
&+ \frac{1}{r}(D_x G u_0(t) \chi_0 D_r - H G u_0(t) \chi_0 D_x ) \psi \\ 
&+ \frac{1}{r}\left( D_r \psi D_x G \psi - D_x \psi H G \psi\right) \\
&+ \frac{1}{r}\left( D_r u_0(t) \chi_0 D_x G u_0(t) \chi_0 - D_x u_0(t) \chi_0 H G u_0(t) \chi_0 \right) \\ 
& + \dot{u}_0(t) G\chi_0 - \frac{u_0(t)}{R} G^2 \chi_0 = 0 
\end{split}
\end{equation}
The boundary conditions become.
\begin{equation}
\begin{split}
& D_r \psi(r,0,t) = 0 \quad D_r D_x \psi(r,1,t) =0 \quad D_x \psi(r_0, x, t) =0 \\
& \frac{1}{r}D_x \psi(0, x, t) \quad \text{and} \quad \frac{1}{r}D_r \psi(0, x, t) \quad \text{finite} 
\end{split} 
\end{equation}
Solve BVP
\begin{equation}
\begin{split}
G \psi_{ij}(r,x) &= \omega_{ij} \psi_{ij}(r,x) \\
\psi_{ij}(r,0,t) &= 0 \quad \frac{\partial \psi_{ij}(r,1,t)}{\partial x}=0 \\ 
\psi_{ij}(0, x, t) & \quad \text{is finite} \quad \psi_{ij}(r_0, x, t)=0 
\end{split}
\end{equation}
Solutions are
\begin{equation}
\begin{split}
\psi_{ij}(r,x) &= r J_1 (j_{1,i} \frac{r}{r_0}) \sin \left( \left(j-\frac{1}{2} \right) \pi x\right) \\ \omega_{ij} &= \left( \frac{j_{1,i}}{r_0} \right)^2 + (2j-1)^2 \frac{\pi^2}{4} \quad i,j \geq 1
\end{split}
\end{equation}
If the number of modes are truncated to $N_x$ axial modes and $N_r$ radial modes. The subscripts $i$ and $j$ can be expressed as a function of a single subscript $n$, such that
\begin{equation}
\begin{split}
\psi_{n}(r,x) &= r J_1 (j_{1,i(n)} \frac{r}{r_0}) \sin \left( \left(j(n)-\frac{1}{2} \right) \pi x\right) \\ \omega_{n} &= \left( \frac{j_{1,i(n)}}{r_0} \right)^2 + (2j(n)-1)^2 \frac{\pi^2}{4}\\
i(n) &= floor(n/N_r) + 1, \quad j(n) = n - i(n) N_r + 1, \quad n \geq 0
\end{split}
\end{equation}
Orthogonality relation is
\begin{multline}
<\psi_{n}(r,x),\psi_{m}(r,x)>=\int_0^{r_0} \int_0^1 \frac{1}{r}\psi_{n}(r,x) \psi_{m}(r,x) dr dx = \Lambda_{n}\delta_{nm}  
\end{multline}

Now revisit equation with inhomogeneous driving term.
\begin{equation}
\begin{split}
& D_t G \psi - \frac{G^2}{R} \psi+ \frac{u_0(t)}{r} ( D_r \chi_0 D_x - D_x \chi_0 H G + D_x G \chi_0 D_r - H G \chi_0 D_x ) \psi \\ 
& +\frac{1}{r}\left( D_r \psi D_x G \psi - D_x \psi H G \psi\right) \\
& +\frac{ u_0(t)^2}{r}\left( D_r \chi_0 D_x G \chi_0 - D_x \chi_0 H G \chi_0 \right) + \dot{u}_0(t) G\chi_0 - \frac{u_0(t)}{R} G^2 \chi_0 = 0
\end{split}
\end{equation}
Assuming $\psi(r,x,t)=\sum_{m} a_{m}(t)\psi_{m}(r,x)$.
\begin{equation}
\begin{split}
& \sum_{m} \left( \omega_{m}\dot{a}_{m}(t) - \frac{\omega_{m}^2a_{m}(t)}{R} \right)\psi_{m} \\
& + \frac{u_0(t)}{r} \sum_{m} a_{m}(t) ( D_r \chi_0 D_x - D_x \chi_0 H G + D_x G \chi_0 D_r - H G \chi_0 D_x ) \psi_{m} \\ 
& +\frac{1}{r} \sum_{m,l} a_{m}(t)a_{l}(t)( D_r \psi_{m} D_x G \psi_{l} - D_x \psi_{m} H G \psi_{l}) \\ 
& +  \frac{ u_0(t)^2}{r}\left( D_r \chi_0 D_x G \chi_0 - D_x \chi_0 H G \chi_0 \right) + \dot{u}_0(t) G \chi_0 - \frac{u_0(t)}{R} G^2 \chi_0 =0
\end{split}
\end{equation}
Taking the inner product of this equation with $\psi_{n}$ 
\begin{equation}
\dot{a}_{n}(t) - \frac{\omega_n}{R}a_n(t) + u_0(t) \sum_{m=0}^N b_{mn} a_{m}(t) + \sum_{l,m=0}^N
c_{lmn} a_{m}(t)a_{l}(t) + u_0(t)^2 d_{n} - \frac{u_0(t)}{R} f_n + \dot{u}_0(t) g_{n}= 0
\end{equation}
\begin{equation}
\begin{split}
\Lambda_{n} &= <\psi_{n}, \psi_{n}> \\
\omega_{n} \Lambda_{n}b_{mn} &= <\psi_{n},\frac{1}{r}( D_r \chi_0 D_x - \omega_{m} D_x \chi_0 H + D_x G \chi_0 D_r - H G \chi_0 D_x ) \psi_{m}> \\
\omega_{n} \Lambda_{n} c_{lmn} &= <\psi_{n}, \frac{\omega_{l} }{r}(D_r \psi_{m} D_x \psi_{l} - D_x \psi_{m} H \psi_{l})> \\
\omega_{n} \Lambda_{n} d_{n} &= <\psi_{n}, \frac{1}{r}(D_r \chi_0 D_x G \chi_0 - D_x \chi_0 H G \chi_0 )> \\
\omega_{n} \Lambda_{n} f_{n} &= <\psi_{n}, G^2 \chi_0> \\
\omega_{n} \Lambda_{n} g_{n} &= <\psi_{n}, G \chi_0>
\end{split}
\end{equation}
\section{Fixed Points}
For the case of constant $u_0$ the fixed points of the system satisfy the equation
\begin{equation}
- \frac{\omega_n}{R}a_n^* + u_0 \sum_{m=0}^N b_{mn} a_{m}^* + \sum_{l,m=0}^N c_{lmn} a_{m}^*a_{l}^* + u_0^2 d_{n} - \frac{u_0}{R} f_n = 0
\end{equation}
For the case of $N_r=1$ and $N_x=1$ this becomes
\begin{equation}
c (a_{0}^*)^2 + \left(u_0 b- \frac{\omega_0}{R} \right) a_0^* + u_0^2 d - \frac{u_0}{R} f = 0
\end{equation}
This has solution
\begin{equation}
a_{0}^* = -\frac{u_0 b- \frac{\omega_0}{R}}{2c} \pm \frac{1}{2c}\sqrt{\left(u_0 b- \frac{\omega_0}{R} \right)^2 - 4 c\left( u_0^2 d - \frac{u_0}{R} f \right)}
\end{equation}
Thus the fixed points exist as long as $b^2-4cd \geq 0$

\end{document}

