\documentclass[12pt, letter]{report}

\usepackage{amsmath, mathrsfs, amssymb}    % need for subequations
\usepackage[pdftex]{graphicx}   % need for figures
\usepackage{verbatim}   % useful for program listings
\usepackage{color}      % use if color is used in text
\usepackage{hyperref}   % use for hypertext links, including those to external documents and URLs
\usepackage{breqn}
\allowdisplaybreaks

\graphicspath{{figs/}}

\begin{document}
The axisymmetric velocity of the jet is defined as $\textbf{u}=(u,v,0)$. The velocities can be expressed in terms of the Stoke's stream function.
\begin{equation}
u = \frac{1}{r} \frac{\partial \Psi}{\partial r} \quad v = - \frac{1}{r} \frac{\partial \Psi}{\partial x}
\end{equation}
The incompressible Euler equation in stream function form can be written as.
\begin{equation}
\frac{\partial }{\partial t} G \Psi + \frac{1}{r}\left( D_r \Psi D_x - D_x \Psi H\right) G \Psi = 0
\end{equation}
The boundary conditions are
\begin{equation}
\begin{split}
D_r \Psi(r,0,t) = u_f(t) r P(r)  \quad & D_r D_x \Psi(r,1,t)=0 \quad D_x \Psi(r_0, x, t)=0 \\
& \text{u(0,x,t), v(0,x,t) finite} 
\end{split} 
\end{equation}
where $G= D^2 - \frac{D}{r} + \frac{\partial^2 }{\partial x^2}$, $H=D - \frac{2}{r}$, $P(r)=\frac{1}{2}\left( 1 - \tanh \left( \frac{1-r}{2 \theta} \right) \right)$ is the jet profile. 

Make the substitution \begin{equation}
\begin{split}
\Psi(r,x,t) &= \psi(r,x,t)+u_0(t) \chi_0(r,x) \\
\chi_0(r,x) &= C_E (x-1)^2 \int r P(r) dr
\end{split}
\end{equation}
This substitution removes the inhomogeneity from the boundary conditions and introduces one into the equation of motion itself.
\begin{equation}
\begin{split}
& \left( D_t + \frac{1}{r} ( D_r u_f(t) \chi_0 D_x - D_x u_f(t) \chi_0 H) \right) G \psi \\
&+ \frac{1}{r}(D_x G u_f(t) \chi_0 D_r - H G u_f(t) \chi_0 D_x ) \psi \\ 
&+ \frac{1}{r}\left( D_r \psi D_x G \psi - D_x \psi H G \psi\right) \\
&+ \frac{1}{r}\left( D_r u_f(t) \chi_0 D_x G u_f(t) \chi_0 - D_x u_f(t) \chi_0 H G u_f(t) \chi_0 \right) \\ 
& + \dot{u}_f(t) G\chi_0 = 0 
\end{split}
\end{equation}
The boundary conditions become.
\begin{equation}
\begin{split}
& D_r \psi(r,0,t) = 0 \quad D_r D_x \psi(r,1,t) =0 \quad D_x \psi(r_0, x, t) =0 \\
& \frac{1}{r}D_x \psi(0, x, t) \quad \text{and} \quad \frac{1}{r}D_r \psi(0, x, t) \quad \text{finite} 
\end{split} 
\end{equation}
Solve BVP
\begin{equation}
\begin{split}
G \psi_{ij}(r,x) &= \omega_{ij} \psi_{ij}(r,x) \\
\psi_{ij}(r,0,t) &= 0 \quad \frac{\partial \psi_{ij}(r,1,t)}{\partial x}=0 \\ 
\psi_{ij}(0, x, t) & \quad \text{is finite} \quad \psi_{ij}(r_0, x, t)=0 
\end{split}
\end{equation}
Solutions are
\begin{equation}
\begin{split}
\psi_{ij}(r,x) &= r J_1 (j_{1,i} \frac{r}{r_0}) \sin \left( \left(j-\frac{1}{2} \right) \pi x\right) \\ \omega_{ij} &= \left( \frac{j_{1,i}}{r_0} \right)^2 + (2j-1)^2 \frac{\pi^2}{4} \quad i,j \geq 1
\end{split}
\end{equation}
If the number of modes are truncated to $N_x$ axial modes and $N_r$ radial modes. The subscripts $i$ and $j$ can be expressed as a function of a single subscript $n$, such that
\begin{equation}
\begin{split}
\psi_{n}(r,x) &= r J_1 (j_{1,i(n)} \frac{r}{r_0}) \sin \left( \left(j(n)-\frac{1}{2} \right) \pi x\right) \\ \omega_{n} &= \left( \frac{j_{1,i(n)}}{r_0} \right)^2 + (2j(n)-1)^2 \frac{\pi^2}{4}\\
i(n) &= floor(n/N_r) + 1, \quad j(n) = n - i(n) N_r + 1, \quad n \geq 0
\end{split}
\end{equation}
Orthogonality relation is
\begin{multline}
<\psi_{n}(r,x),\psi_{m}(r,x)>=\int_0^{r_0} \int_0^1 \frac{1}{r}\psi_{n}(r,x) \psi_{m}(r,x) dr dx = \Lambda_{n}\delta_{nm}  
\end{multline}

Now revisit equation with inhomogeneous driving term.
\begin{equation}
\begin{split}
& D_t G \psi + \frac{u_f(t)}{r} ( D_r \chi_0 D_x G - D_x \chi_0 H G + D_x G \chi_0 D_r - H G \chi_0 D_x ) \psi \\ 
& +\frac{1}{r}\left( D_r \psi D_x G \psi - D_x \psi H G \psi\right) \\
& +\frac{ u_f(t)^2}{r}\left( D_r \chi_0 D_x G \chi_0 - D_x \chi_0 H G \chi_0 \right) + \dot{u}_f(t) G\chi_0 = 0
\end{split}
\end{equation}
Assuming $\psi(r,x,t)=\sum_{m} a_{m}(t)\psi_{m}(r,x)$.
\begin{equation}
\begin{split}
& \sum_{m} \omega_{m}\dot{a}_{m}(t)\psi_{m} \\
& + \frac{u_f(t)}{r} \sum_{m} a_{m}(t) ( D_r \chi_0 D_x G - D_x \chi_0 H G + D_x G \chi_0 D_r - H G \chi_0 D_x ) \psi_{m} \\ 
& +\frac{1}{r} \sum_{m,l} a_{m}(t)a_{l}(t)( D_r \psi_{m} D_x G \psi_{l} - D_x \psi_{m} H G \psi_{l}) \\ 
& +  \frac{ u_f(t)^2}{r}\left( D_r \chi_0 D_x G \chi_0 - D_x \chi_0 H G \chi_0 \right) + \dot{u}_f(t) G \chi_0 =0
\end{split}
\end{equation}
Taking the inner product of this equation with $\psi_{n}$ 
\begin{equation}
\label{eq:stream_modes}
\dot{a}_{n}(t) + u_0(t) \sum_{m=0}^{N_a} B_{mn} a_{m}(t) + \sum_{l,m=0}^{N_a}
C_{lmn} a_{m}(t)a_{l}(t) + u_0(t)^2 d_{n} + \dot{u}_0(t) f_{n}= 0
\end{equation}
\begin{equation}
\begin{split}
\Lambda_{n} &= <\psi_{n}, \psi_{n}> \\
B_{mn} &= \frac{1}{\omega_{n} \Lambda_{n}}<\psi_{n},\frac{1}{r}( \omega_{m} D_r \chi_0 D_x  - \omega_{m} D_x \chi_0 H + D_x G \chi_0 D_r - H G \chi_0 D_x ) \psi_{m}> \\
 C_{lmn} &= \frac{1}{\omega_{n} \Lambda_{n}} <\psi_{n}, \frac{\omega_{l} }{r}(D_r \psi_{m} D_x \psi_{l} - D_x \psi_{m} H \psi_{l})> \\
 d_{n} &= \frac{1}{\omega_{n} \Lambda_{n}} <\psi_{n}, \frac{1}{r}(D_r \chi_0 D_x G \chi_0 - D_x \chi_0 H G \chi_0 )> \\
 f_{n} &= \frac{1}{\omega_{n} \Lambda_{n}} <\psi_{n}, G \chi_0>
\end{split}
\end{equation}
\section{Connection to Acoustic Flow}
\begin{equation}
\label{eq:u_dot}
\dot{u}_0 = \mu^{-1} \left(p - p_{src} + \sum_{i}^{N_q} \dot{q}_i -\frac{\gamma}{2} u_{0}^2  \right)
\end{equation}
\begin{equation}
\label{eq:acoustic}
\vec{\ddot{q}} + \boldsymbol{D}' \vec{\dot{q}} + \frac{\gamma }{\mu^2} u_0 \vec{B}' \cdot \vec{\dot{q}} + \boldsymbol{K}' \vec{q} = \vec{B}' \frac{\gamma }{\mu^2} u_0 p_{src} - \vec{B}' \frac{\gamma}{\mu} \dot{p}_{src}  -\left(\vec{A}' + \vec{B}'  \frac{\gamma }{\mu^2}p \right) u_0 + \vec{B}'  \frac{\gamma^2 }{2\mu^2} u_{0}^3,
\end{equation} 
The pressure source due to vorticity is given by
\begin{equation}
p_{src} = \frac{1}{2 \pi r_0^2} \int_V \left( \textbf{r} \times \frac{\partial \boldsymbol{\omega}}{\partial t}  \right)_x dV
\end{equation}
Writing out the cross product and volume element in cylindrical coordinates,
\begin{equation}
p_{src} = \frac{1}{2 \pi r_0^2} \int_{0}^{r_0}\int_{0}^{1} r^2 \frac{\partial \omega_{\phi}(r,x,t)}{\partial t} dr dx
\end{equation}
In the previous section recall the stream function was expressed as
\begin{equation}
\Psi(r,x,t) = \sum_n a_n(t) \psi_n (r,x) + u_0(t) \chi (r,x)
\end{equation}
The azimuthal component of the vorticity is given by $\omega_{\phi} = -\nabla^2 \Psi(r,x,t)$. Thus, its time derivative is given by 
\begin{equation} 
\frac{\partial \omega_{\phi}}{\partial t} = - \sum_n \dot{a}_n(t) \nabla^2 \psi_n (r,x) - \dot{u}_0(t) \nabla^2 \chi (r,x)
\end{equation}
Thus the pressure source can be expressed as 

\begin{equation}
\begin{split}
p_{src} &= -\sum_i \zeta_i \dot{a}_i - \dot{u}_0 \eta \\
\zeta_i &= \frac{1}{r_0^2}\int_0^{r_0}\int_0^1 r^2 \nabla^2 \psi_i(r,x) dr dx \\
\eta &= \frac{1}{r_0^2}\int_0^{r_0}\int_0^1 r^2 \nabla^2 \chi(r,x) dr dx
\end{split}
\end{equation}
Substituting Eq. \ref{eq:stream_modes},
\begin{equation}
p_{src} = u_0 \sum_{m,n=0}^{N_a} B_{mn} \zeta_n a_{m} + \sum_{l,m,n=0}^{N_a}
C_{lmn} \zeta_n a_{m} a_{l} + u_0^2 \sum_{n=0}^{N_a} d_{n} \zeta_n + \dot{u}_0 \left( \eta + \sum_{n=0}^{N_a} f_{n} \zeta_n \right)
\end{equation}
The substituting in Eq. \ref{eq:u_dot} for the $\dot{u}_0$ term and solving for $p_{src}$.
\begin{multline}
p_{src} = u_0 \sum_{m,n=0}^{N_a} B_{mn} \zeta_n a_{m} + \sum_{l,m,n=0}^{N_a} C_{lmn} \zeta_n a_{m} a_{l} + u_0^2 \sum_{n=0}^{N_a} \left(d_{n} \zeta_n + \frac{\gamma}{2\mu} \left( \eta + \sum_{n=0}^{N_a} f_{n} \zeta_n \right) \right) + \\ \mu^{-1} \left(p - p_{src} + \sum_{i}^{N_q} \dot{q}_i \right) \left( \eta + \sum_{n=0}^{N_a} f_{n} \zeta_n \right)
\end{multline}
Solving this for $p_{src}$ and writing it in vector form
\begin{multline}
\label{eq:p_{src}}
p_{src} = \frac{u_0}{1 + \frac{\eta + \vec{f} . \vec{\zeta}}{\mu}} \textbf{B} . \vec{a} . \vec{\zeta}  + \frac{u_0^2}{1 + \frac{\eta + \vec{f} . \vec{\zeta}}{\mu} } \left( \left(\vec{d}+\frac{\gamma}{2\mu}\vec{f} \right) . \vec{\zeta} + \frac{\gamma}{2\mu} \eta \right) + \frac{\eta + \vec{f} . \vec{\zeta}}{\mu+\eta + \vec{f} . \vec{\zeta}} \left(p + \sum_{i}^{N_q} \dot{q}_i \right) \\ 
+ \frac{1}{1 + \frac{\eta + \vec{f} . \vec{\zeta}}{\mu}}\sum_{l,m,n=0}^{N_a} C_{lmn} \zeta_n a_{m} a_{l}
\end{multline}
Now taking the time derivative of this expression
\begin{multline}
\dot{p}_{src} =\frac{\dot{u}_0 \textbf{B} . \vec{a} . \vec{\zeta}}{1 + \frac{\eta + \vec{f} . \vec{\zeta}}{\mu}}  + \frac{u_0 \textbf{B} . \vec{\dot{a}} . \vec{\zeta}}{1 + \frac{\eta + \vec{f} . \vec{\zeta}}{\mu}}  + \frac{2u_0 \dot{u}_0 \left(\frac{\gamma \eta}{2\mu} + \left(\vec{d}+\frac{\gamma}{2\mu}\vec{f} \right) \right) . \vec{\zeta}}{1 + \frac{\eta + \vec{f} . \vec{\zeta}}{\mu}} + \frac{\eta + \vec{f} . \vec{\zeta}}{\mu+\eta + \vec{f} . \vec{\zeta}}\sum_{i}^{N_q} \ddot{q}_i \\ + \frac{1}{1 + \frac{\eta + \vec{f} . \vec{\zeta}}{\mu}}\sum_{l,m,n=0}^{N_a} C_{lmn} \zeta_n (\dot{a}_{m} a_{l}+a_{m} \dot{a}_{l})=F(u_0, \dot{u}_0, \vec{a}, \vec{\dot{a}}, r_f, p) + \frac{\eta + \vec{f} . \vec{\zeta}}{\mu+ \eta + \vec{f} . \vec{\zeta}}\sum_{i}^{N_q} \ddot{q}_i
\end{multline}
Substituting these expressions into Eq. \ref{eq:acoustic}
\begin{equation}
\label{eq:acoustic2}
\textbf{R}^{-1} \vec{\ddot{q}}+ \left(\boldsymbol{D}' + \vec{B}' \frac{\gamma }{\mu^2} u_0 \right) \vec{\dot{q}} + \boldsymbol{K}' \vec{q} =\vec{B}' \frac{\gamma }{\mu^2} u_0 p_{src} - \vec{B}' \frac{\gamma}{\mu} F(u_0, \dot{u}_0, \vec{a}, \vec{\dot{a}}, r_f, p)   -\left(\vec{A}' + \vec{B}'  \frac{\gamma }{\mu^2}p \right) u_0 + \vec{B}'  \frac{\gamma^2 }{2\mu^2} u_{0}^3,
\end{equation} 
where
\begin{equation}
R_{jk}^{-1} = \delta_{jk} -B_j' \frac{\gamma (\eta + \vec{f} . \vec{\zeta})}{\mu(\mu+ \eta + \vec{f} . \vec{\zeta})}
\end{equation}
Multiplying Eq. \ref{eq:acoustic2} by the matrix $\textbf{R}$,
\begin{equation}
\label{eq:acoustic2}
\vec{\ddot{q}}+ \left(\boldsymbol{D} + \vec{B} \frac{\gamma }{\mu^2} u_0 \right) \vec{\dot{q}} + \boldsymbol{K} \vec{q} =\vec{B} \frac{\gamma }{\mu^2} u_0 p_{src} - \vec{B} \frac{\gamma}{\mu} F(u_0, \dot{u}_0, \vec{a}, \vec{\dot{a}}, r_f, p)   -\left(\vec{A} + \vec{B}  \frac{\gamma }{\mu^2}p \right) u_0 + \vec{B}  \frac{\gamma^2 }{2\mu^2} u_{0}^3,
\end{equation} 
\end{document}
