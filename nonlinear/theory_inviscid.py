import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy.special import j0, j1, jn, jn_zeros
from scipy.integrate import odeint
from scipy.optimize import fsolve, root
import matplotlib.animation as animation
from cmath import sqrt as csqrt
from math import floor, pi
plt.style.use('bmh')

j = csqrt(-1)

#Nr=1, Nx=4, L=0.004, r0=0.003/L, rf=0.001/L, beta=1.5625

L = 0.004
uc = 343
rho = 1.43
r0 = 0.003 / L
dx = 0.01
dr = 0.01
x = np.arange(0, 1+dx, dx)
r = np.arange(1e-20, r0+dr, dr)

#martrix.shape=(x,r)
def inner(f1, f2):
	return np.sum(f1 * f2 * dx * dr / r )

def z1_to_z2(n, Nr):
	""" 
	Converts the index of a 1d array to a 2d array with Nr columns.
	These indices start 1.
	"""
	
	i = int(floor(n/Nr))
	j = n - i * Nr

	return i, j

def calc_psi(n, Nr):
	i, j = z1_to_z2(n, Nr)
	i += 1
	j += 1
	j1j = jn_zeros(1, j)[-1]
	R = r * j1(j1j * r / r0)
	X = np.sin((i-0.5)*pi*x)

	return np.outer(X, R)

def calc_psi_dr(n, Nr):
	i, j = z1_to_z2(n, Nr)
	i += 1
	j += 1
	j1j = jn_zeros(1, j)[-1]
	R = j1(j1j * r / r0) - j1j * r / 2 / r0 * ( j1(j1j * r / r0) - jn(2, j1j * r / r0) ) 
	X = np.sin((i-0.5)*pi*x)

	return np.outer(X, R)

def calc_psi_dr2(n, Nr):
	i, j = z1_to_z2(n, Nr)
	i += 1
	j += 1
	a = jn_zeros(1, j)[-1] / r0
	R = a * ( j0(a*r) - 0.75 * a * r * j1(a*r) - jn(2, a*r) + 0.25 * a * r * jn(3, a*r) )  
	X = np.sin((i-0.5)*pi*x)

	return np.outer(X, R)

def calc_psi_dx(n, Nr):
	i, j = z1_to_z2(n, Nr)
	i += 1
	j += 1
	j1j = jn_zeros(1, j)[-1]
	R = r * j1(j1j * r / r0)
	X = (i-0.5) * pi * np.cos((i-0.5)*pi*x)

	return np.outer(X, R)

def calc_psi_dx2(n, Nr):
	i, j = z1_to_z2(n, Nr)
	i += 1
	j += 1
	j1j = jn_zeros(1, j)[-1]
	R = r * j1(j1j * r / r0)
	X = -(i-0.5)**2 * pi**2 * np.sin((i-0.5)*pi*x)

	return np.outer(X, R)

def calc_omega(n, Nr):
	i, j = z1_to_z2(n, Nr)
	i += 1
	j += 1
	j1j = jn_zeros(1, j)[-1]
	return j1j **2 / r0**2 + (2*i-1)**2 * pi**2 / 4

def calc_chi(rf, beta=1.625):
	delta = rf / 10
	#b = 0.25 * rf / delta
	P = 0.5 * (1 + np.tanh(beta * (rf/r - r/rf)))
	R = np.cumsum(r * P * dr)
	X = (x-1)**2
	chi = np.outer(X, R)
	chi_dr = np.outer(X, r * P)
	chi_G = Dr(chi_dr) - chi_dr / r + Dx(Dx(chi))
	chi_H = chi_dr + chi / r

	return chi, chi_dr, chi_G, chi_H  

def Dr(f):
	return np.gradient(f)[1] / dr

def Dx(f):
	return np.gradient(f)[0] / dx

def G(f):
	return Dr(Dr(f)) - Dr(f) / r + Dx(Dx(f))

def H(f):
	return Dr(f) - 2 * f / r

def calc_Lambda(n, Nr):
	psi_n = calc_psi(n, Nr)

	return inner(psi_n, psi_n)

def calc_b(rf, beta, m, n, Nr):
	chi, chi_dr, chi_G, chi_H = calc_chi(rf, beta)
	psi_m = calc_psi(m, Nr)
	psi_n = calc_psi(n, Nr)
	omega_m = calc_omega(m, Nr)
	omega_n = calc_omega(n, Nr)
	Lambda_n = calc_Lambda(n, Nr)
	right = (omega_m * chi_dr * Dx(psi_m) - omega_m * Dx(chi) * H(psi_m) + 
		Dx(chi_G) * Dr(psi_m) - H(chi_G) * Dx(psi_m) ) / r

	return inner(psi_n, right) / omega_n / Lambda_n

def calc_c(rf, beta, l, m, n, Nr):
	chi, chi_dr, chi_G, chi_H = calc_chi(rf, beta)
	psi_l = calc_psi(l, Nr)
	psi_m = calc_psi(m, Nr)
	psi_n = calc_psi(n, Nr)
	omega_l = calc_omega(l, Nr)
	omega_n = calc_omega(n, Nr)
	Lambda_n = calc_Lambda(n, Nr)
	right = omega_l / r * (Dr(psi_m) * Dx(psi_l) - Dx(psi_m) * H(psi_l) )

	return inner(psi_n, right) / omega_n / Lambda_n

def calc_d(rf, beta, n, Nr):
	chi, chi_dr, chi_G, chi_H = calc_chi(rf, beta)
	psi_n = calc_psi(n, Nr)
	omega_n = calc_omega(n, Nr)
	Lambda_n = calc_Lambda(n, Nr)
	right = (chi_dr * Dx(chi_G) - Dx(chi) * H(chi_G)) / r

	return inner(psi_n, right) / omega_n / Lambda_n

def calc_bcd(rf, beta, N, Nr):
	b = np.zeros((N, N))
	c = np.zeros((N, N, N))
	d = np.zeros(N)

	for n in range(N):
		d[n] = calc_d(rf, beta, n, Nr)
		for m in range(N):
			b[m,n] = calc_b(rf, beta, m, n, Nr)
			for l in range(N):
				c[l,m,n] = calc_c(rf, beta, l, m, n, Nr)

	return b, c, d

def flow(a, t, *args):
	b, c, d, u0 = args

	N = len(a)

	a_quadratic = np.outer(a,a)

	linear = np.dot(b.T, a)
		
	nonlinear = np.array([sum(c[:,:,n] * a_quadratic) for n in range(N)])

	forcing = np.array([d[n] for n in range(N)])

	adot = -u0 * linear - nonlinear - u0**2 * forcing

	return adot.flatten()

def unique_rows(arr, precision):
	return np.vstack({tuple(row) for row in np.around(arr, precision)})

def astar_polynomial(a, *args):
	b, c, d, u0 = args

	if type(a[0]) is scipy.optimize.optimize.OptimizeResult:
		a = a[0]['x']

	a = a[:N] + j * a[N:]

	a_quadratic = np.outer(a, a)

	linear = np.dot(b.T, a)	

	nonlinear = np.array([sum(c[:,:,n] * a_quadratic) for n in range(N)])

	forcing = np.array([d[n] for n in range(N)])

	polynomial = u0 * linear + nonlinear + u0**2 * forcing
		
	return np.hstack((polynomial.real, polynomial.imag))

def astar_1d(b, c, d, u0):
	fp1 = -u0*b/2/c + u0/2/c*np.sqrt(b**2 - 4 * c * d + 0*j)
	fp2 = -u0*b/2/c - u0/2/c*np.sqrt(b**2 - 4 * c * d + 0*j)
	
	return fp1, fp2

def calc_fixed_points(b, c, d, u0, tol=1e-9, num_seeds = 100 ):
	size = 10.
	N = len(d)
	seeds = [size*np.random.rand(2*N)-size/2 for _ in range(num_seeds)]
	a_star_info = np.vstack([root(astar_polynomial, a0, args=(b, c, d, u0), tol=1e-17) for a0 in seeds])
	a_star = np.array([asi[0]['x'] for asi in a_star_info])
	precision = np.array([astar_polynomial(a, b, c, d, u0) for a in a_star])
	a_star = a_star[abs(np.mean(precision,1)) < tol]
	a_star = unique_rows(a_star, 6) #unique rows
		
	return a_star

def find_real_fixed_points(a_star):
	idx = sum(a_star[:,N:],1)==0

	return a_star[idx,:N]

def calc_jacobian(a0, b, c, d, u0):
	jac = -u0 * b - sum([(c[i,:,:]+c[:,i,:])*a0[i] for i in range(len(c))] , 0)
	
	return jac

def calc_jacobian_evals(a0, b, c, d, u0):
	jac = calc_jacobian(a0, b, c, d, u0)
	
	return np.linalg.eigvals(jac)

def calc_vorticity(psi, psi_dr, psi_dr_2, psi_dx_2):
	"""Del^2 f = drr_f + dr_f / r + dxx_f. f=f(x, r) 

	TODO: Dr(psi)!= psi_dr
	"""

	return -(psi_dr_2 + psi_dr / r + psi_dx_2)

if __name__ == '__main__':
	rf = 0.0009 / L
	beta = 10
	Nr = 2
	Nx = 4
	N = Nx * Nr

	b, c, d = calc_bcd(rf, beta, N, Nr)
	chi, chi_dr, chi_G, chi_H = calc_chi(rf, beta)
	u0 = 0.1

	"""
	a_star = calc_fixed_points(b, c, d, u0, tol=1e-10, num_seeds=200)
	a_star_real = find_real_fixed_points(a_star)
	evals = [calc_jacobian_evals(a0, b, c, d, u0) for a0 in a_star_real]
	#[plot(ev.real, ev.imag, 'o') for ev in evals]
	"""

	T = 1000
	dt = 0.1
	t_range = np.arange(0, T, dt)
	a0 = np.zeros(N)
	a = odeint(flow, a0, t_range, args=(b, c, d, u0) )
	adot = np.vstack([flow(a[tn,:], t, b, c, d, u0) for tn, t in enumerate(t_range)])

	psi_n = np.array([calc_psi(n, Nr) for n in range(N)])
	psi_n_dr = np.array([calc_psi_dr(n, Nr) for n in range(N)])
	psi_n_dr2 = np.array([calc_psi_dr2(n, Nr) for n in range(N)])
	psi_n_dx = np.array([calc_psi_dx(n, Nr) for n in range(N)])
	psi_n_dx2 = np.array([calc_psi_dx2(n, Nr) for n in range(N)])

	#calculate force
	calc_psi_dt = lambda tn: sum([adot[tn, n] * psi_n[n,:,:] for n in range(N)], axis=0)
	calc_psi_dr_dt = lambda tn: sum([adot[tn, n] * psi_n_dr[n,:,:] for n in range(N)], axis=0)
	calc_omega_dt = lambda tn: cylindrical_laplacian(calc_psi_dt(tn), calc_psi_dr_dt(tn))
	calc_f_omega_t = lambda tn: pi * sum(calc_omega_dt(tn) * r**2 * dr * dx)

	f_omega_t = np.array([calc_f_omega_t(tn) for tn in range(len(t_range))])

	fig, ax = plt.subplots(2)
	ax[0].plot(t_range*L/uc*1000, a)
	ax[0].set_xlabel('t (ms)')
	ax[0].set_ylabel('a_n')
	ax[1].plot(t_range*L/uc*1000, f_omega_t * rho * uc**2 / pi / r0**2 )
	ax[1].set_xlabel('t (ms)')
	ax[1].set_ylabel('p (Pa)')
	ax[1].set_title('Voriticty Presure (x component)')

	#animate vorticity
	calc_psi_t = lambda tn: sum([a[tn, n] * psi_n[n,:,:] for n in range(N)], axis=0)
	calc_psi_dr_t = lambda tn: sum([a[tn, n] * psi_n_dr[n,:,:] for n in range(N)], axis=0)
	calc_psi_dx_t = lambda tn: sum([a[tn, n] * psi_n_dx[n,:,:] for n in range(N)], axis=0)
	calc_psi_dr2_t = lambda tn: sum([a[tn, n] * psi_n_dr2[n,:,:] for n in range(N)], axis=0)
	calc_psi_dx2_t = lambda tn: sum([a[tn, n] * psi_n_dx2[n,:,:] for n in range(N)], axis=0)
	calc_omega_t = lambda tn: calc_omega(calc_psi_t(tn), calc_psi_dr_t(tn), 
		calc_psi_dr2_t(tn), calc_psi_dx2_t(tn))

fig_animation, ax_animation = plt.subplots()
ffx = 5
omega_t_0 = calc_omega_t(0)
im = ax_animation.imshow(omega_t_0.T, origin='lower', vmin=-0.4, vmax=0.5)
fig_animation.colorbar(im)
ax_animation.set_xlabel('x')
ax_animation.set_ylabel('r')
ax_animation.set_title('azimuthal vorticity')
def animate(tn):
	tn *= ffx
	print(tn, t_range[tn]/1000)
	im.set_array(calc_omega_t(tn).T)
	#fig_animation.canvas.draw()

	return im,

ani = animation.FuncAnimation(fig_animation, animate, frames=len(t_range)/ffx, interval=1, blit=True)

fig_vx, ax_vx = plt.subplots()
ffx = 5
vx_t_0 = calc_psi_dr_t(0) / r 
im_vx = ax_vx.imshow(vx_t_0.T, origin='lower', vmin=-0.1, vmax=0.2)
fig_vx.colorbar(im_vx)
ax_vx.set_xlabel('x')
ax_vx.set_ylabel('r')
ax_vx.set_title('azimuthal vorticity')
def animate_vx(tn):
	tn *= ffx
	print(tn, t_range[tn]/1000)
	vx_t = calc_psi_dr_t(tn) / r
	im_vx.set_array(vx_t.T)

	return im_vx,

ani = animation.FuncAnimation(fig_vx, animate_vx, frames=len(t_range)/ffx, interval=1, blit=True)

fig_vr, ax_vr = plt.subplots()
ffx = 5
vr_t_0 = -calc_psi_dx_t(0) / r 
im_vr = ax_vr.imshow(vr_t_0.T, origin='lower', vmin=-0.1, vmax=0.2)
fig_vr.colorbar(im_vr)
ax_vr.set_xlabel('x')
ax_vr.set_ylabel('r')
ax_vr.set_title('azimuthal vorticity')
def animate_vr(tn):
	tn *= ffx
	print(tn, t_range[tn]/1000)
	vr_t = -calc_psi_dx_t(tn) / r
	im_vr.set_array(vr_t.T)

	return im_vr,

ani = animation.FuncAnimation(fig_vr, animate_vr, frames=len(t_range)/ffx, interval=1, blit=True)

ffx = 10
X, R = np.meshgrid(x, r)
vx_t_0 = calc_psi_dr_t(0) / r 
vr_t_0 = -calc_psi_dx_t(0) / r 
fig_v_field, ax_v_field = plt.subplots()
v_field = ax_v_field.quiver(X, R , vx_t_0.T, vr_t_0.T)
ax_v_field.set_xlabel('x')
ax_v_field.set_ylabel('r')
ax_v_field.set_title('velocity field')

def animate_v_field(tn):
	tn *= ffx
	print(tn, t_range[tn]/1000)
	vx_t = calc_psi_dr_t(tn) / r 
	vr_t = -calc_psi_dx_t(tn) / r 
	ax_v_field.cla()
	v_field = ax_v_field.quiver(X, R , vx_t.T, vr_t.T)

	return v_field,

ani = animation.FuncAnimation(fig_v_field, animate_v_field, frames=len(t_range)/ffx, interval=1, blit=True)
