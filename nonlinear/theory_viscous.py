import numpy as np
import scipy
from scipy.special import j1, jn_zeros
from scipy.integrate import odeint
from scipy.optimize import fsolve, root
from cmath import sqrt as csqrt
plt.style.use('bmh')

j = csqrt(-1)

Nr = 2
Nx = 1
N = Nx * Nr
L = 0.01
r0 = 0.003 / L
rf = 0.6
dx = 0.001
dr = 0.001
x = np.arange(0, 1+dx, dx)
r = np.arange(1e-20, r0+dr, dr)

#martrix.shape=(x,r)
def inner(f1, f2):
	return np.sum(f1 * f2 * dx * dr / r )

def z1_to_z2(n, Nr):
	""" 
	Converts the index of a 1d array to a 2d array with Nr columns.
	These indices start 1.
	"""
	
	i = int(floor(n/Nr))
	j = n - i * Nr

	return i, j

def calc_psi(n):
	i, j = z1_to_z2(n, Nr)
	i += 1
	j += 1
	j1j = jn_zeros(1, j)[-1]
	R = r * j1(j1j * r / r0)
	X = np.sin((i-0.5)*pi*x)

	return np.outer(X, R)

def calc_omega(n):
	i, j = z1_to_z2(n, Nr)
	i += 1
	j += 1
	j1j = jn_zeros(1, j)[-1]
	return j1j **2 / r0**2 + (2*i-1)**2 * pi**2 / 4

def calc_chi(rf):
	delta = rf / 10
	b = 0.25 * rf / delta
	P = 0.5 * (1 + tanh(b * (rf/r - r/rf)))
	R = np.cumsum(r * P * dr)
	X = (x-1)**2
	chi = np.outer(X, R)
	chi_dr = np.outer(X, r * P)
	chi_G = Dr(chi_dr) - chi_dr / r + Dx(Dx(chi))
	chi_GG = Dr(chi_G) - chi_dr / r + Dx(Dx(chi))
	chi_H = chi_dr + chi / r

	return chi, chi_dr, chi_G, chi_H  

def Dr(f):
	return np.gradient(f)[1] / dr

def Dx(f):
	return np.gradient(f)[0] / dx

def G(f):
	return Dr(Dr(f)) - Dr(f) / r + Dx(Dx(f))

def H(f):
	return Dr(f) - 2 * f / r

def calc_Lambda(n):
	psi_n = calc_psi(n)

	return inner(psi_n, psi_n)

def calc_b(rf, m, n):
	chi, chi_dr, chi_G, chi_H = calc_chi(rf)
	psi_m = calc_psi(m)
	psi_n = calc_psi(n)
	omega_m = calc_omega(m)
	omega_n = calc_omega(n)
	Lambda_n = calc_Lambda(n)
	right = (chi_dr * Dx(psi_m) - omega_m * Dx(chi) * H(psi_m) + 
		Dx(chi_G) * Dr(psi_m) - H(chi_G) * Dx(psi_m) ) / r

	return inner(psi_n, right) / omega_n / Lambda_n

def calc_c(rf, l, m, n):
	chi, chi_dr, chi_G, chi_H = calc_chi(rf)
	psi_l = calc_psi(l)
	psi_m = calc_psi(m)
	psi_n = calc_psi(n)
	omega_l = calc_omega(l)
	omega_n = calc_omega(n)
	Lambda_n = calc_Lambda(n)
	right = omega_l / r * (Dr(psi_m) * Dx(psi_l) - Dx(psi_m) * H(psi_l) )

	return inner(psi_n, right) / omega_n / Lambda_n

def calc_d(rf, n):
	chi, chi_dr, chi_G, chi_H = calc_chi(rf)
	psi_n = calc_psi(n)
	omega_n = calc_omega(n)
	Lambda_n = calc_Lambda(n)
	right = (chi_dr * Dx(chi_G) - Dx(chi) * H(chi_G)) / r

	return inner(psi_n, right) / omega_n / Lambda_n

def calc_f(rf, n):
	chi, chi_dr, chi_G, chi_H = calc_chi(rf)
	psi_n = calc_psi(n)
	omega_n = calc_omega(n)
	Lambda_n = calc_Lambda(n)
	right = (chi_dr * Dx(chi_G) - Dx(chi) * H(chi_G)) / r

	return inner(psi_n, right) / omega_n / Lambda_n

def calc_bcd(rf, N):
	b = np.zeros((N, N))
	c = np.zeros((N, N, N))
	d = np.zeros(N)

	for n in range(N):
		d[n] = calc_d(rf, n)
		for m in range(N):
			b[m,n] = calc_b(rf, m,n)
			for l in range(N):
				c[l,m,n] = calc_c(rf, l,m,n)

	return b, c, d

def flow(a, t, *args):
	b, c, d, u0 = args

	N = len(a)

	a_quadratic = np.outer(a,a)

	linear = np.dot(b.T, a)
		
	nonlinear = np.array([sum(c[:,:,n] * a_quadratic) for n in range(N)])

	forcing = np.array([d[n] for n in range(N)])

	adot = -u0 * linear - nonlinear - u0**2 * forcing

	return adot.flatten()

from IPython import embed

def astar_polynomial(a, *args):
	b, c, d, u0 = args

	if type(a[0]) is scipy.optimize.optimize.OptimizeResult:
		a = a[0]['x']

	a = a[:N] + j * a[N:]

	a_quadratic = np.outer(a, a)

	linear = np.dot(b.T, a)	

	nonlinear = np.array([sum(c[:,:,n] * a_quadratic) for n in range(N)])

	forcing = np.array([d[n] for n in range(N)])

	polynomial = u0 * linear + nonlinear + u0**2 * forcing
		
	return np.hstack((polynomial.real, polynomial.imag))

def astar_1d(b, c, d, u0):
	fp1 = -u0*b/2/c + u0/2/c*np.sqrt(b**2 - 4 * c * d + 0*j)
	fp2 = -u0*b/2/c - u0/2/c*np.sqrt(b**2 - 4 * c * d + 0*j)
	
	return fp1, fp2

def calc_fixed_points(b, c, d, u0):
	num_seeds = 100 
	size = 10.
	N = len(d)
	seeds = [size*np.random.rand(2*N)-size/2 for _ in range(num_seeds)]
	a_star_info = np.vstack([root(astar_polynomial, a0, args=(b, c, d, u0), tol=1e-17) for a0 in seeds])
	a_star = np.array([asi[0]['x'] for asi in a_star_info])
	#a_star = np.vstack({tuple(row) for row in np.around(a_star, 6)}) #unique rows
	#precision = np.array([astar_polynomial(a, b, c, d, u0) for a in a_star])
	#a_star = a_star[precision[:,0]<1e-9]
	#a_star = np.vstack([fsolve(astar_polynomial, a0, args=(b, c, d, u0)) for a0 in a_star])
	#

	return a_star

rf = 0.7 * r0
u0 = 0.01
b, c, d = calc_bcd(rf, N)
a_star = calc_fixed_points(b, c, d, u0)
np.array([astar_polynomial(a, b, c, d, u0) for a in a_star])


T = 1000
dt = 0.01
t = np.arange(0, T, dt)
a0 = np.zeros(N)
a = odeint(flow, a0, t, args=(b, c, d, u0) )
plot(a)

psi_n= calc_psi(0)

rf_range = np.arange(0, r0, 0.01)
b_rf = np.array([calc_b(rf, 0, 0) for rf in rf_range])
c_rf = np.array([calc_c(rf, 0, 0, 0) for rf in rf_range])
d_rf = np.array([calc_d(rf, 0) for rf in rf_range])

fig, ax = plt.subplots()
ax.plot(rf_range/r0, b_rf**2-4*c_rf*d_rf)
ax.set_ylabel('discriminant')
ax.set_xlabel('r_jet / r_pipe')

def nullspace(A, atol=1e-13, rtol=0):
    A = np.atleast_2d(A)
    u, s, vh = np.linalg.svd(A)
    tol = max(atol, rtol * s[0])
    nnz = (s >= tol).sum()
    ns = vh[nnz:].conj().T
    return ns

M = np.hstack( ( u0*b, np.hstack(c) ) )
ns = nullspace(M)

u, s, vh = np.linalg.svd(A)
b1 , b2 = vh[0,:], vh[1,:]

