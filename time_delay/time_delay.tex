\documentclass[12pt, letter]{report}

\usepackage{amsmath}    % need for subequations
\usepackage[pdftex]{graphicx}   % need for figures
\usepackage{verbatim}   % useful for program listings
\usepackage{xcolor}      % use if color is used in text
\usepackage{caption}  
\usepackage{subcaption}  % use for side-by-side figures
\usepackage{hyperref}   % use for hypertext links, including those to external documents and URLs
\usepackage{setspace}
\usepackage{listings}
\usepackage{rotating}
%\usepackage{breqn}

\graphicspath{{../figs/}}


\begin{document}
\begin{equation}
\label{eq:wave}
\begin{split}
\frac{\partial^2 \phi}{\partial x^2} - \frac{\partial^2 \phi}{\partial t^2} = 0 \\ 
\frac{\partial \phi (0,t)}{\partial x} = u_0 (t, \phi(0,t), \dot{\phi}(0,t)) \\ 
\frac{\partial \phi (1,t)}{\partial t} + Z \frac{\partial \phi (1,t)}{\partial x} = 0 \\
\frac{\partial}{\partial x} \phi(x,0)=0
\end{split}
\end{equation}

\begin{equation}
\tau = x_r + 1
\end{equation}

\begin{equation}
\label{eq:viscous2}
\ddot{q}_m + \beta_m \dot{q_m} + \omega_m^2 q_m = -a_m R u_0(t - \tau) +b_m R \ddot{u}_0(t - \tau).
\end{equation}

\begin{equation}
\phi_m(x) = \cosh(s_m x).
\end{equation}

\section{The Flow Through the Vocal Folds}
The system given by Eq. \ref{eq:hysteretic_damping} is a set of time domain damped oscillator equation, whose driving term depends on $u_0$ and its derivatives. For the system to be a set of fully solvable ODEs, the driving velocity must be expressed in terms of the tracheal pressure and the geometry of the vocal folds. The velocity of the vocal fold flow is on the order of $30 \thinspace \frac{m}{s}$, and the vocal radius is on the order of $1 \thinspace mm$. So the Reynold's number of the vocal fold flow is on the order of $2000$. Thus, it's valid to expect the flow through the vocal folds to be conservative. Therefore it is valid to use Bernoulli's equation to express energy conservation of the flow between the trachea ($T$) and pharyngeal end of the glottis ($G$). Fig. \ref{fig:vocal_folds} shows a diagram of the flow through the vocal folds, with the points $T$ and $G$ marked.  
\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{vocal_folds.png}
\caption{Schematic of the flow through the vocal folds. The subglottal pressure focuses air into a jet as it is forced through the vocal folds. The velocity $u_0$, along with additional pressure sources $\Delta p_{src}$ emerging from the folds drives resonances in the upper vocal tract. The pressure, at the origin, $p(0,t)$associated with these resonances provides a feedback mechanism that opposes $u_0$. }
\label{fig:vocal_folds}
\end{figure}
\begin{equation}
\label{eq:energy1}
(p_{P} - p_{T}) + \frac{1}{2}(u_{P}^2 - u_{T}^2) + \frac{\partial}{\partial t} (\phi_{P} - \phi_{T}) = 0.
\end{equation}  

The flow through the vocal folds is also assumed to be incompressible. There are no precise measurements for the thickness of the vocal folds. However the shortest acoustic wavelengths emitted by the rodent vocal tract are on the order of $4 \thinspace mm$. Incompressibility is a good assumption as long the vocal fold thickness is less than half that. Since the vocal fold radius is about $1 \thinspace mm$, this is reasonable even for the highest frequency modes. Thus, from continuity of mass and the incompressibility of the flow, $u_{T}=\frac{A_{P}}{A_{T}}u_{P}$. Furthermore, the change in velocity potential can be expressed as $\phi_{P} - \phi_{T} = \int_{T}^{P} u(x) dx$, where $u(x)$ is the axially varying flow velocity inside the folds. Using conservation of mass again this can be written in terms of the velocity at the glottal end $\phi_{P} - \phi_{T} = A_{P} u_{P} \int_{T}^{P} \frac{dx}{A(x)}=$, where $A(x)$ is the axially varying area of the folds. Inserting these equations into Eq. \ref{eq:energy1},
\begin{equation}
\label{eq:energy2}
(p_{P} - p_{T}) + \frac{1}{2} u_{P}^2 \left(1 - \left( \frac{A_{P}}{A_{T}} \right)^2 \right) + \frac{\partial}{\partial t} \left( A_{P} u_{P} \int_{T}^{P} \frac{dx}{A(x)} \right) = 0.
\end{equation} 
The velocity $u_{P}$ will be fed into the upper vocal tract exciting acoustic resonances. This is the feed-forward element of the system. 

After a short distance the flow emerging from the vocal folds will mix with the acoustic flow of the upper vocal tract. Conservation of mass can be used again to relate the flow emerging from the vocal folds to $u_0$, the driving velocity in Eqs. \ref{eq:viscous2} and \ref{eq:hysteretic_damping}, $A_{P} u_{P} = u_0 A_0$. Inserting this into Eq. \ref{eq:energy2},
\begin{equation}
\label{eq:energy3}
(p_{P} - p_{T}) + \frac{1}{2} u^2 \left( \left( \frac{A_{0}}{A_{P}} \right)^2- \left( \frac{A_{0}}{A_{T}} \right)^2 \right) + \frac{\partial}{\partial t} \left( A_{0} u \int_{T}^{P} \frac{dx}{A(x)} \right) = 0.
\end{equation} 
By continuity of pressure $p_{P}$ must equal the acoustic pressure response generated in the upper vocal tract at $x=0$ plus the pressure from any additional sources driving the acoustic flow. Thus, $p_{P}=p(x_r,t)=-\frac{\partial \phi(x_r,t)}{\partial t}$. This is the feedback condition, since the driving velocity $u_0$ will excite a pressure response in the resonator, which will impede further flow. The nature of $\Delta p_{src}$ will be discussed more later. Again by continuity of pressure $p_{T}$ must equal the pressure in the trachea. This is the input to the system, and it will just be called $p$, with the understanding that it can be made to vary in time. Inserting these equations into Eq. \ref{eq:energy3}, 
\begin{equation}
\label{eq:energy4}
p(x_r,t) - p + \frac{1}{2} u^2 \left( \left( \frac{A_{0}}{A_{P}} \right)^2- \left( \frac{A_{0}}{A_{T}} \right)^2 \right) + \frac{\partial}{\partial t} \left( A_{0} u \int_{T}^{P} \frac{dx}{A(x)} \right) = 0.
\end{equation}
This equation can be simplified by defining the defining the area rations $C_E = \frac{A_{0}}{A_{P}}$ (expansion coefficient) and $C_C = \frac{A_G}{A_t}$ (contraction coefficient.
\begin{equation}
\label{eq:energy5}
p(x_r,t) - p + \frac{C_E^2}{2} u^2 \left( 1 - C_C^2 \right) + \frac{\partial}{\partial t} \left( A_{0} u \int_{T}^{P} \frac{dx}{A(x)} \right) = 0.
\end{equation}

The derivative term can also be simplified by assuming a profile for the vocal folds. Since detailed measurements of the geometry of the rat vocal folds don't exist, there is little point at this time to attempt to provide precise numerical predictions. Instead it is of more value to provide mechanistic understanding of the acoustic production mechanism. With this in mind the vocal folds can be assumed to have a linear converging profile. In this case, the axially varying area can be written as 
\begin{equation}
\label{eq:area1}
A(x)= \left(\sqrt{A_G}-\sqrt{A_{GT}}\right)^2 \left(\frac{x}{l}  \right)^2- 2 \sqrt{A_{GT}} \left(\sqrt{A_{GT}}-\sqrt{A_G}\right)\frac{x}{l}+A_{GT},
\end{equation}
where $l$ is the length of the vocal folds and $A_{GT}$ is the area of the vocal folds on the tracheal side. Eq. \ref{eq:area1} simplifies somewhat if it is written in terms of $C_F = \frac{A_{TG}}{A_G}$ (the fold coefficient), the area ratio of the tracheal and pharyngeal sides of the vocal folds.
\begin{equation}
\label{eq:area2}
\frac{A(x)}{A_G} = \left( 1-\sqrt{C_F} \right)^2 \left( \frac{x}{l} \right)^2 + 2\left( \sqrt{C_F} - C_F \right) \frac{x}{l} + C_F.
\end{equation}
Evaluating the integral in Eq. \ref{eq:energy4}
\begin{equation}
A_0 \int_{T}^{P} \frac{dx}{A(x)} = C_E  \frac{l}{\sqrt{C_F}} = C_E l_e
\end{equation}
where $l_e =  \frac{l}{\sqrt{C_F}}$ (the effective length of the vocal folds). An even further simplification is to assume to the profile of the vocal folds is constant. In this case, $C_F=1$, and the effective length equals the actual length of the vocal folds, $l_e=l$. Inserting these expressions into Eq. \ref{eq:energy4},
\begin{equation}
\label{eq:energy6}
-\frac{\partial \psi(x_r, t)}{\partial t} - p + \frac{1}{2} u^2 C_E^2 \left( 1 - C_C^2 \right) + \left( C_E l_e + \frac{(x_r - 1)^2}{2}\right) \dot{u}_{0} = 0.
\end{equation} 
Is is also reasonable to assume the time derivatives of $C_E$ and $l_e$ are negligible compared to those of $u_0$ and $q_n$, since the acoustic oscillations have a much higher frequency than is physically possible for the vocal folds to maintain. Thus, we can take them out of the time derivative in Eq. \ref{eq:energy5}

\section{Closing the System}
\begin{equation}
\dot{u}_{0} = \mu^{-1} ( -\frac{\gamma}{2} u^2 + \frac{\partial \psi(x_r, t)}{\partial t} + p )
\end{equation}

\begin{equation}
\ddot{u}_{0} = \frac{-\gamma}{\mu} u \dot{u}_0 + \mu^{-1} \frac{\partial^2 \psi(x_r, t)}{\partial t^2} 
\end{equation}

\begin{equation}
\ddot{u}_{0} = -\frac{\gamma p}{\mu^2} + \frac{\gamma^2}{2 \mu^2} u^3 - \frac{\gamma}{\mu^2} u \frac{\partial \psi(x_r, t)}{\partial t} + \frac{1}{\mu}\frac{\partial^2 \psi(x_r, t)}{\partial t^2} 
\end{equation}


\begin{equation}
\ddot{u}_{0}(t - \tau ) = -\frac{\gamma p(t - \tau ) u(t - \tau)}{\mu^2} + \frac{\gamma^2}{2 \mu^2} u(t - \tau )^3 - \frac{\gamma}{\mu^2} u(t-\tau) \sum_l \cosh(s_l x_r) \dot{q}_l(t-\tau)  + \frac{1}{\mu} \sum_l \cosh(s_l x_r) \ddot{q}_l(t-\tau)  
\end{equation}

\begin{equation}
\ddot{u}_{0}(t - \tau ) = -\frac{\gamma p(t - \tau ) u(t - \tau)}{\mu^2} + \frac{\gamma^2}{2 \mu^2} u(t - \tau )^3 -\frac{\gamma}{\mu^2} u(t-\tau) \sum_l c_l \dot{q}_l(t-\tau)  + \frac{1}{\mu} \sum_l c_l \ddot{q}_l(t-\tau)  
\end{equation}

\begin{equation}
\label{eq:viscous2}
\ddot{q}_m + \beta_m \dot{q_m} + \omega_m^2 q_m = -a_m R u_0(t - \tau) +b_m R \ddot{u}_0(t - \tau).
\end{equation}

\begin{multline}
\ddot{q}_m + \beta_m \dot{q_m} + \omega_m^2 q_m = -R \left( a_m + \frac{b_m \gamma p(t-\tau)}{\mu^2} \right) u_0(t - \tau) + 
\\ b_m R \left(  \frac{\gamma^2}{2 \mu^2} u(t - \tau )^3 -\frac{\gamma}{\mu^2} u(t-\tau) \sum_l c_l \dot{q}_l(t-\tau)  + \frac{1}{\mu} \sum_l c_l \ddot{q}_l(t-\tau) \right)
\end{multline}

\section{Frequency Domain Analysis}
\begin{equation}
\dot{u} = \mu^{-1} (-\frac{\gamma}{2} u^2 + \sum_l c_l \dot{q}_l + p(t) )
\end{equation}
\begin{equation}
\label{eq:viscous2}
\ddot{q}_m + \beta_m \dot{q_m} + \omega_m^2 q_m = -a_m R u(t - \tau) +b_m R \ddot{u}(t - \tau).
\end{equation}
Fixed point
\begin{equation}
U = \sqrt{\frac{2p}{\gamma}}
\end{equation}
Define 
\begin{equation}
u = v + U
\end{equation}
Linearize
\begin{equation}
\dot{u} = \mu^{-1} (\sum_l c_l \dot{q}_l + p(t) )
\end{equation}
\begin{equation}
\label{eq:viscous2}
\ddot{q}_m + \beta_m \dot{q_m} + \omega_m^2 q_m = -a_m R u(t - \tau) +b_m R \ddot{u}(t - \tau).
\end{equation}
Fourier Transform
\begin{equation}
i \omega U = \mu^{-1} (\sum_l c_l i \omega Q_l + P(\omega) )
\end{equation}
\begin{equation}
\label{eq:viscous2}
- \omega^2 Q_m + \beta_m i \omega Q_m + \omega_m^2 Q_m = -a_m R U(t - \tau) +b_m R s^2 U(t - \tau).
\end{equation}


\section{Multi Time Scales Analysis}
Scale parameters by $\epsilon$
\begin{equation}
\dot{u}_{0} = \mu^{-1} ( -\epsilon \frac{\hat{\gamma}}{2} u^2 + \sum_l c_l \dot{q}_l + \epsilon \hat{p} )
\end{equation}

\begin{multline}
\ddot{q}_m + \epsilon \hat{\beta}_m \dot{q_m} + \omega_m^2 q_m = -\epsilon R \left( \hat{a}_m + \frac{\hat{b}_m \gamma p(t-\tau)}{\mu^2} \right) u_0(t - \tau) + 
\\ \hat{b}_m \epsilon R \left(  \frac{\gamma^2}{2 \mu^2} u(t - \tau )^3 -\frac{\gamma}{\mu^2} u(t-\tau) \sum_l c_l \dot{q}_l(t-\tau)  + \frac{1}{\mu} \sum_l c_l \ddot{q}_l(t-\tau) \right)
\end{multline}
Expand solutions and derivatives in powers of $\epsilon$
\begin{equation}
\begin{split}
u &=   u_{0}(T_0, T_1) + \epsilon u_{1}(T_0, T_1) \\
q_m &=  q_{m0}(T_0, T_1) + \epsilon q_{m1}(T_0, T_1) \\
\frac{\partial }{\partial t} &= D_0 + \epsilon D_1 \\
\frac{\partial^2 }{\partial t^2} &= D_0^2 + \epsilon 2 D_0 D_1 
\end{split}
\end{equation}

\begin{equation}
\begin{split}
\left( u_{0} + \epsilon u_{1} \right)^2 &= u_{0}^2 + \epsilon 2 u_{0} u_{1} + ...  \\
\left( u_{0} + \epsilon u_{1} \right)^3 &= u_{0}^3 + \epsilon u_{0}^2 u_{1} + ... \\
\left( D_0 + \epsilon D_1 \right)\left( q_{m0} + \epsilon q_{m1} \right) &= D_0 q_{m0} + \epsilon \left( D_0 q_{m1} + D_1 q_{m0} \right) + .. \\ 
\left( u_{0} + \epsilon u_{1} \right) \left( D_0 + \epsilon D_1 \right)\left( q_{m0} + \epsilon q_{m1} \right) &= u_{0} D_0 q_{m0} + \epsilon \left( u_{0} D_0 q_{m1} + u_{0} D_1 q_{m0} + u_{1} D_0 q_{m0} \right) ... \\
\dot{q}_m = \left( D_0 + \epsilon D_1 \right) \left( q_{m0} + \epsilon q_{m1} \right) &= D_0 q_{m0} + \epsilon \left( D_0 q_{m1} + D_1 q_{m0} \right) + ... \\
\ddot{q}_m = \left( D_0^2 + \epsilon 2 D_0 D_1 \right) \left( q_{m0} + \epsilon q_{m1} \right) &= D_0^2 q_{m0} + \epsilon \left( D_0^2 q_{m1} + 2 D_0 D_1 q_{m0}  \right) + ...
\end{split}
\end{equation}

To order 0
\begin{equation}
\begin{split}
D_0 u_{0} - \frac{1}{\mu} \sum_l c_l D_0 q_{l0} &= 0 \\
D_0^2 q_{m0} + \omega_m^2 q_{m0} &= 0
\end{split}
\end{equation}
To order $\epsilon$
\begin{equation}
\begin{split}
D_0 u_{1} - \frac{1}{\mu} \sum_l c_l  D_0 q_{m1}  &= F_0(u_{0}, q_{m0})  \\
D_0^2 q_{m1}  + \omega_m^2 q_{m1} &= F_m(u_{0}, q_{m0}) 
\end{split}
\end{equation}

\begin{equation}
\begin{split}
F_0 &= \frac{1}{\mu} \sum_l c_l D_1 q_{m0}(T_0, T_1) - D_1 u_{0} - \frac{\hat{\gamma}}{2 \mu}u_{0}(T_0, T_1)^2 + \frac{\hat{p}}{\mu} \\
F_m &= -2 D_0 D_1 q_{m0}(T_0, T_1) - \hat{\beta}_m D_0 q_{m0}(T_0, T_1) - R \left( \hat{a}_m +\frac{\hat{b}_m \gamma p}{\mu^2} \right) u_{0}(T_0 - \tau, T_1 - \epsilon \tau) \\
& -\hat{b}_m R ( -u_{0}(T_0 - \tau, T_1 - \epsilon \tau)^3 + \frac{\gamma}{\mu^2} u_{0}(T_0 - \tau, T_1 - \epsilon \tau) \sum_l c_l D_0 q_{l0}(T_0 - \tau, T_1 - \epsilon \tau) - \\ & \frac{1}{\mu} \sum_l c_l D_0^2 q_{l0}(T_0 - \tau, T_1 - \epsilon \tau) )
\end{split}
\end{equation}
The equations to order 0 can be solved analytically.
\begin{equation}
\begin{split}
q_{m0}(T_0, T_1) &= A_m(T_1) e^{i \omega_m T_0} + A_m^*(T_1) e^{-i \omega_m T_0} \\
u_{0}(T_0, T_1) &= A_0(T_1) + \frac{1}{\mu} \sum_l c_l \left( A_m(T_1) e^{i \omega_m T_0} + A_m^*(T_1) e^{-i \omega_m T_0} \right) 
\end{split}
\end{equation}
Subbing these in
\begin{multline}
F_0(u_{0}, q_{m0}) = \\ 
\frac{\hat{p}}{\mu}-\dot{A}_0 - \frac{\hat{\gamma}}{2 \mu}\left( A_0^2 +  \left( \frac{1}{\mu} \sum_l c_l \left( A_m(T_1) e^{i \omega_m T_0} + A_m^*(T_1) e^{-i \omega_m T_0} \right) \right)^2 + \frac{2 A_0}{\mu} \sum_l c_l \left( A_m(T_1) e^{i \omega_m T_0} + A_m^*(T_1) e^{-i \omega_m T_0} \right)\right)
\end{multline}
The solvability condition is 
\begin{equation}
\frac{\hat{p}}{\mu}-\dot{A}_0 - \frac{\gamma}{2 \mu}\left( A_0^2 + \sum_l 2 c_l^2 |A_l|^2 \right)=0
\end{equation}

The rest of the problem is intractable for arbitrary $N$. I'll consider the case for $N=2$.
\begin{equation}
\begin{split}
-2 D_0 D_1 q_{m0}(T_0, T_1) &= -2 i \omega_m \left(\dot{A}_m e^{i \omega_m T_0} - \dot{A}_m e^{-i \omega_m T_0}  \right) \\
- \hat{\beta}_m D_0 q_{m0}(T_0, T_1) &= - \hat{\beta}_m i \omega_m \left( \left(\dot{A}_m e^{i \omega_m T_0} - \dot{A}_m e^{-i \omega_m T_0}  \right) \right) \\
- R \left( \hat{a}_m +\frac{\hat{b}_m \gamma p}{\mu^2} \right) u_{0}(T_0 - \tau, T_1 - \epsilon \tau) &= - R \left( \hat{a}_m +\frac{\hat{b}_m \gamma p}{\mu^2} \right) \left( A_0(T_1-\epsilon \tau) + \frac{1}{\mu} \sum_l c_l \left( A_l(T_1-\epsilon \tau) e^{i \omega_l (T_0-\tau)} + A_l^*(T_1-\epsilon \tau) e^{-i \omega_l (T_0-\tau)} \right) \right)
\hat{b}_m R u_{0}(T_0 - \tau, T_1 - \epsilon \tau)^3 = \hat{b}_m R
\end{split}
\end{equation}

\end{document}