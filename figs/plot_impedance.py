from pylab import *
from scipy.special import struve, jv
plt.style.use('bmh')

def X(x):
	return struve(1, 2*x) / x

def R(x):
	return 1 - jv(1, 2*x) / x

x = np.arange(0.0001, 10, 0.01)

fig, ax = subplots()
ax.loglog(x, X(x), label='X(ka)')
ax.loglog(x, R(x), label='R(ka)')
ax.set_xlabel('ka')
ax.set_title('Impedance of a Baffled Circular Piston')
ax.legend(loc=4)