from matplotlib import pyplot as plt
import numpy as np
from jump_interface import JumpInterface
import curve_fit as cf
import poly_cluster as pc
import minimize_cost as mc
import bootstrap
plt.style.use('bmh')

ji = JumpInterface('./jump.db')

RAT_CLUSTERS = {'V1':[3,8], 'V2':[3,8], 'V3':[3,8], 'V4':[1,2,3,8,9,10],
		'V5':[3,8], 'V6':[1,2,3,8,9,10], 'V15':[1,3,8,10], 'V17':[3,8], 
		'V18':[3,8], 'V31':[1,3,8,10], 'V32': [3,8]}

rat = 'V6'
included_clusters = RAT_CLUSTERS[rat]
plt.close('all')
for rat, included_clusters in RAT_CLUSTERS.iteritems():
jumps = ji.get_jumps(rat=rat)[:,0:2]

gamma_min = mc.minimize_cost(jumps, initial = [-0.6], included_clusters=included_clusters)
cost_min = mc.cost([gamma_min], jumps, included_clusters)
gamma_range = np.arange(-1, 1, 0.01)
cost = np.array([mc.cost([gamma], jumps, included_clusters) for gamma in gamma_range])
fig_cost, ax_cost = plt.subplots()
fig_cost, ax_cost, lines_cost = mc.plot_cost(gamma_range, cost, gamma_min, cost_min, rat)
ax_cost.set_title(rat + ' Cost Function and Local Minimum')
fig_cost.savefig('./jump_figs/'+ rat +'_cost.png')

slopes = pc.calc_slopes(gamma_min, included_clusters)
bisecting_slopes = pc.find_bisector(slopes)
vertices = pc.polygon_vertices(bisecting_slopes, included_clusters)
cluster = pc.poly_cluster(jumps, vertices)
fig_jumps, ax_jumps, line_jumps = pc.plot_jump_frequencies(jumps, cluster)
ax_jumps.set_title(rat + ' Clustered Jump Frequencies')
fig_jumps.savefig('./jump_figs/'+ rat +'_jumps.png')