\Chapter{Appendix}
\section{Analysis}
The Git repository \url{https://gitlab.com/mdornfe1/vocalization_analysis} contains an SQLite database with the $9598$ stored recordings and a collection of Python scripts for their analysis. The database contains several thousand recordings. Uncompressed it is about $9$ GBs, so it may take a few minutes to download. To run the code in this section you must have git, Python 2.7, and the Python package virtualenv installed. To download the data and analysis scripts clone the repository with git and activate the Python virtual environment (which contains the necessary Python packages). On a Unix system this accomplished with the following commands.
\begin{lstlisting}
git clone \
https://gitlab.com/mdornfe1/vocalization_analysis.git
cd ./vocalization_analysis
source venv/bin/activate
\end{lstlisting}
With the virtual environment activated, import the modules which will use in the analysis. Also the Bayesian Methods for Hackers style sheet will be used because it much more visually appealing than the default matplotlib style sheet.
\begin{lstlisting}[language=Python]
from matplotlib import pyplot as plt
import numpy as np
from jump_interface import JumpInterface
import curve_fit as cf
import poly_cluster as pc
import minimize_cost as mc
import bootstrap
plt.style.use('bmh')
\end{lstlisting}

\subsection{Working With The Data}
To interact with the recordings stored in jump.db the JumpInterface class can be used. To create an instance of JumpInterface pass the path of the database to its constructor. The constructor will create a connection to the database.
\begin{lstlisting}[language=Python]
ji = JumpInterface('./jump.db') 
\end{lstlisting}
With the connection established the methods of JumpInterface can be used to pull data from the database. The names of the rats can be retrieved using the get\_rats method.
\begin{lstlisting}[language=Python]
rats = ji.get_rats()
print(rats)
>>>array([u'V1', u'V15', u'V17', u'V18', u'V2', u'V3', 
	u'V31', u'V32', u'V4', u'V5', u'V6'], 
	dtype='<U3')
\end{lstlisting}
The audio recordings are stored in the table signals.The print\_column\_names() method prints the columns layout of a table in the database.
\begin{lstlisting}[language=Python]
ji.print_column_names(table='signals')
>>>Table Name: signals
   0 signal_index INTEGER
   1 rat TEXT
   2 Fs REAL
   3 signal ARRAY
\end{lstlisting}
Each recording is a Numpy array stored in the column signal and is indexed by primary key signal\_index. Also stored in this table is the rat that each recording came from and the sampling frequency Fs at which the recording was made. The rat column is useful for gettting all vocalizations made by an individual rat. The sampling frequency column is necessary for getting the frequency content of a vocalization. To retrieve an individual recording and the sampling frequency at which it was recorded use the get\_signal and get\_fs methods, passing them the appropriate value of signal\_index. For example,
\begin{lstlisting}[language=Python]
signal = ji.get_signal(signal_index=10)
fs = ji.get_fs(signal_index=10) 
\end{lstlisting}
These recordings can most easily be visualized though their reassigned spectrogram. The file curve\_fit.py contains functions for calculating and plotting the reassigned spectrogram of an audio recording.
\begin{lstlisting}[language=Python]
mgram = cf.calc_mgram(signal)
fig, ax, im = cf.plot_mgram(mgram, fs)
plt.show()
\end{lstlisting}
The output of this code block is shown in Fig. \ref{fig:spectrogram_10}.
\begin{figure}
\centering
\includegraphics[width=\linewidth]{spectrogram_10.png}
\caption{Reassigned spectrogram of rat USV. Frequency jumps can be seen at approximately $23.0$ ms and $43.0$ ms after the start of the recording.}
\label{fig:spectrogram_10}
\end{figure}

\subsection{Curve Fitting and Jump Point Extraction}
By examining Fig. \ref{fig:spectrogram_10} it can see that this vocalization mostly consists of a signal frequency modulated tone. Of particular interest to this analysis are the frequency jump points, at which the rat discontinuously changes its frequency of vocalization from one value to another. This analysis is largely concerned with extracting these jump points from the recordings and fitting that data to an acoustic model. To extract the jump points use the fit\_curve function in curve\_fit.py to fit a single value curve to the vocalization in spectrogram space. The function works by finding the mean frequency weighted by the energy in each frequency bin, for each time time slice in spectrogram space. Fig. \ref{fig:mean_freq} illustrates the curve fitting algorithm at a few selected time points. At each time point the power distribution is treated as a probability distribution. The maximum of that distribution is found, then average of the three surrounding frequencies is computed weighted by the power in each frequency. 
\begin{figure}
\centering
\includegraphics[width=\linewidth]{mean_freq.png}
\caption{Power plotted against frequency for the time points $5.0$ ms, $10.0$ ms, $15.0$ ms of the spectrogram shown in Fig. \ref{fig:spectrogram_10}. The time point $5.0$ ms shows the power distribution of noise. Although this particular time point has low entropy it will be ignored by the jump extraction algorithm because it is in a high entropy region.}
\label{fig:mean_freq}
\end{figure}

The function fit\_curve performs the process shown in Fig. \ref{fig:mean_freq} at every time point in the recording. It returns three arrays: s, ts, and valid. It also returns one float average\_h. The array s is the fitted frequency. The array ts gives the times at which the frequencies are fitted. The array valid is a binary array. A value of 1 signifies the time slice has a signal with entropy below a certain threshold (meaning it is not noisy). A value of 0 signifies the time slice has entropy above the threshold, hence it is not valid. The float average\_h gives the average entropy of the recording and is a measure of its overall quality. The function plot\_curve plots the fitted curve on top of the spectrogram (Fig. \ref{fig:fitted_curve}).  
\begin{lstlisting}[language=Python]
s, ts, valid, average_h = cf.fit_curve(mgram, fs)
fig, ax, curve_line = cf.plot_curve(s, ts, fs, fig, ax)
plt.show()
\end{lstlisting}
\begin{figure}
\centering
\includegraphics[width=\linewidth]{spectrogram_curve_10.png}
\caption{Spectrogram with fitted curve obtained by using the fit\_curve function.}
\label{fig:fitted_curve}
\end{figure}

It can be seen that the fit to the recorded vocalization is rather good albeit imperfect. However it is only important that the fit corresponds well at the frequency jump points, which can be seen to be true. The function calc\_jump\_freqs extracts the jump frequencies from the fitted curve by calculating the rates of change of the curve from one time slice to the next. The function label from scipy.ndimage.measurements is then used to classify points as a jump points or not based on the instantaneous rate of change. The jump points can be plotted on top of the fitted curve and spectrogram. The array jumps gives the before and after jump frequencies and times. The columns of the array jumps are given by $[f_{before}, f_{after}, t_{before}, t_{after}]$.
\begin{lstlisting}[language=Python]
jumps = cf.calc_jump_freqs(s, ts, valid)
fig, ax, jumps_line = cf.plot_jumps(jumps, fs, fig, ax)
plt.show()
\end{lstlisting}
\begin{figure}
\centering
\includegraphics[width=\linewidth]{spectrogram_curve_jumps_10.png}
\caption{Spectrogram, fitted curve, and extracted jump points. The algorithm rejects the jump point at $t=43.0$ ms because the fitted curve varies to much in that region, but that does not pose a problem to our analysis since it is not a systemic error.}
\label{fig:fitted_curve}
\end{figure}
It can be seen that the jump extraction algorithm is quite accurate. It gives very few false positives. The algorithm rejects the jump point at $t=43.0$ ms because the fitted curve varies to much in that region, but that does not pose a problem to our analysis since it is not a systemic error. For exploration of the data, there is no need to rerun the jump extraction or curve fitting algorithms, because it is already done! The fitted curves are stored in the database in the table curves. The column layout of the table curves can be see with the print\_column\_names method. 
\begin{lstlisting}[language=Python]
ji.print_column_names(table='curves')
>>>Table Name: curves
   0 signal_index INTEGER
   1 rat TEXT
   2 curve ARRAY
\end{lstlisting}
The one dimensional arrays s and ts are stored in the two dimensional array curve. The table curves uses the same primary key signal\_index as the table signals. This way the fitted curve corresponding to a given recording can easily be retrieved from the database. Also stored is the name of the rat the recording came from, so all data for a given rat can be retrieved at once.

The column layout of the table jumps can also be printed.
\begin{lstlisting}[language=Python]
ji.print_column_names(table='jumps')
>>>Table Name: jumps
   0 jump_index INTEGER
   1 rat 
   2 f1 REAL
   3 f2 REAL
   4 t1 REAL
   5 t2 REAL
   6 signal_index INTEGER
   7 cluster INTEGER
   8 quality INTEGER
\end{lstlisting}
This table has primary key jump\_index. Each value of jump\_index corresponds to a single extracted jump point from a recording. The information about the jump point is stored in columns two through five. The columns f1 and f2 respectively store the before and after jump frequencies. The columns t1 and t2 respectively store the times (measured from the beginning of the recording in seconds) at which the before and after jump frequencies were recorded. Also stored are the signal\_index (column 6) of the recording from which the jump point was extracted and the rat (column 1) from which the recording was made from. The columns cluster and quality are not used and can be ignored.

The method get\_jump\_indices can be used to find values of jump\_index associated with a given signal and then select the stored jump frequencies from the table jumps.
\begin{lstlisting}[language=Python]
curve = ji.get_curve(signal_index=10)
s, ts = np.split(curve, [1], 1)
jump_indices = ji.get_jump_indices(signal_index=10)
jumps = np.array([ji.get_jump(i) for i in jump_indices])
\end{lstlisting}

\subsection{The Clustering Algorithm}
Our goal in this analysis is to take all of the jump points extracted from recordings for a given rat and fit them to an equation of the form of Eq. \ref{eq:freq_jump}. Successfully fitting the data to such an equation will give further evidence that the source of rat USVs is an aerodynamic whistle. Furthermore, in doing so an estimation for the parameter $\gamma$ can be obtained, which will gives information about the type of whistle this mechanism resembles.

To accomplish this goal it is helpful to retrieve all jump points from the database for an individual rat and plotting the after jump frequencies against the before jump frequencies. The file poly\_cluster.py contains functions for plotting the jump frequencies and clustering them according to Eq. \ref{eq:freq_jump}. This procedure can be performed with the below code, which generates the plot shown in Fig. \ref{fig:plot_jumps}.
\begin{lstlisting}[language=Python]
jumps_V6 = ji.get_jumps(rat='V6')[:,0:2]
fig_jumps, ax_jumps, line_jumps = (
	pc.plot_jump_frequencies(jumps_V6))
plt.show()
\end{lstlisting}
\begin{figure}
\centering
\includegraphics[width=\linewidth]{plot_jumps.png}
\caption{}
\label{fig:plot_jumps}
\end{figure}

With a visual examination of Fig. \ref{fig:plot_jumps} it can be seen that the jump points are separated into two large groups. The upper group consists of upward frequency jumps, while the lower group consists of downward frequency jumps. Within these two large groups, it can be seen there are three subgroupings. Each subgrouping seems to consist of points clustered along a straight line. This is exactly what would be expected from Eq. \ref{eq:freq_jump}, the interpretation begin that each subgroup corresponds to a mode transition $n \rightarrow n \pm 1$. In this interpretation the slopes of each line that the points group themselves into is given by the right side of Eq. \ref{eq:freq_jump}. By clustering these points according to this equation an estimation can be obtained for the parameter $\gamma$. 

The calc\_slopes functions can be used to calculate the slopes given by Eq. \ref{eq:freq_jump}. The list included\_clusters specifies which mode transitions to calculate the slopes for. The elements of included\_clusters must be integers. The variable LEGEND in the file globals.py is a lookup table between these integers and the corresponding mode transition. For example if the integer $1$ is an element of included\_clusters then calc\_slopes will calculate the slope for the $n=2$ to the $n=1$ mode transition. The following code calculates the slopes for transitions between the $n=1,2,3$ modes and plots the corresponding straight lines on top of the frequency jump points. For this example, a value of $\gamma$ was chosen at semi-random. The output is shown \ref{fig:plot_jumps_lines}.
\begin{lstlisting}[language=Python]
included_clusters = [1, 2, 3, 8, 9, 10]
slopes = pc.calc_slopes(gamma = -0.5, 
	included_clusters=included_clusters)
fig_jumps, ax_jumps, line_jumps = (
	pc.plot_jump_frequencies(jumps_V6))
fig_jumps, ax_jumps, line_jumps = (
	pc.plot_lines(slopes, fig_jumps, ax_jumps))
plt.show()
\end{lstlisting}
\begin{figure}
\centering
\includegraphics[width=\linewidth]{plot_jumps_lines.png}
\caption{Extracted jump frequencies for rat V6. Mode lines from Eq. \ref{eq:freq_jump} are plotted over the jump frequencies. Our goal is to assign each jump point to the closest mode line.}
\label{fig:plot_jumps_lines}
\end{figure}   

Our goal is to group the jump points into appropriate mode transition clusters. This is done by assigning a point to the line for which the perpendicular distance between the two objects is minimal. This is most easily done using the point in polygon algorithm. To do this the bisectors of the above lines are found and polygons are drawn so that their vertices lie on the bisecting lines. The function find\_bisector finds the slopes of the lines which bisect the above shown mode transition lines. The function polygon\_vertices then returns the vertices of four sided polygons which lie on those bisecting lines. The function plot\_polygons then plots the polygons with those vertices. The output of this code is shown in Fig. \ref{fig:point_in_poly}. 
\begin{lstlisting}[language=Python]
bisecting_slopes= pc.find_bisector(slopes)
vertices = pc.polygon_vertices(bisecting_slopes, 
	included_clusters)
fig_jumps, ax_jumps, line_jumps = (
	pc.plot_jump_frequencies(jumps_V6))
fig_jumps, ax_jumps, polygons = (
	pc.plot_polygons(
    vertices, slopes, fig_jumps, ax_jumps))
\end{lstlisting}
\begin{figure}
\centering
\includegraphics[width=\linewidth]{point_in_poly.png}
\caption{Illustration of how the point in polygon algorithm is used to cluster the jump points. Each polygon corresponds to a mode line from Eq. \ref{eq:freq_jump}. The mode lines run down the center of their associated polygons. Jump points are assigned to the polygon in which they lie.}
\label{fig:point_in_poly}
\end{figure}   

A point can now be assigned to a mode transition cluster according to which polygon it lies in. The point in polygon algorithm, which is part of the matplotlib package can be quickly used to assigned a jump point to the a mode transition cluster. By examining Figs. \ref{fig:plot_jumps_lines} and \ref{fig:point_in_poly} it can be seen that the clustering obtained for this value of $\gamma$ roughly corresponds to the natural breaks in density of the points plotted in this space. However, a better clustering result can be obtained by finding an optimal value of $\gamma$. To do this a cost function is defined as the sum of the squares of the perpendicular distances of each point to its assigned cluster line divided by the number of points in each cluster.
\begin{align}
\label{eq:cost}
C\left(\gamma\right) &= \sum_{c}\frac{1}{N_{c}}\sum_{i_c=1}^{N_{c}}\left(\Delta x_{i_c}\right)^{2}+\left(\Delta y_{i_c}\right)^{2} \\
\Delta x_{i_{c}} &= \left\vert f_{1,i_{c}}-\frac{f_{1,i_{c}}+m\left(c,\gamma\right)f_{2,i_{c}}}{m\left(c,\gamma\right)^{2}+1}\right\vert
\\ \Delta y_{i_c} &= \left\vert f_{2,i_{c}}-\frac{m\left(c,\gamma\right)\left(f_{1,i_{c}}+m\left(c,\gamma\right)f_{2,i_{c}}\right)}{m\left(c,\gamma\right)^{2}+1}  \right\vert
\end{align}
In this expression $\Delta x_{i_c}$ and $\Delta y_{i_c}$ are the respective horizontal and vertical distances of the $i^{th}$ point in cluster $c$ to its assigned line, and $N_{c}$ is the number of points in cluster $c$. Each cluster $c$ corresponds to one type of mode transition $n \rightarrow n\pm1$. The quantities $f_{2,i_c}$ and $f_{1,i_c}$ are the after and before jump frequencies of the $i^{th}$ point in cluster $c$. The quantity $m(c, \gamma)$ is the slope of the mode line for cluster $c$ and given value of $\gamma$ as defined in Eq. \ref{eq:freq_jump}.

The script minimize\_cost contains functions which find a value of $\gamma$ that minimizes $C(\gamma)$. The somewhat redundantly named function minimize\_cost returns the minimizing value of gamma. The function cost calculates the value of the cost function for a given value of $\gamma$ and a selection of jump points. The function plot\_cost then plots the cost function and the minimum value of $\gamma$. The following code finds the minimizing value of $\gamma$ and plots the function $C(\gamma)$ (Fig. \ref{fig:cost_plot}).
\begin{lstlisting}[language=Python]
gamma_min = mc.minimize_cost(jumps_V6, 
	initial = [-0.5], 
  included_clusters=included_clusters)
cost_min = mc.cost([gamma_min], 
	jumps, included_clusters)
gamma_range = np.arange(-1, 1, 0.01)
cost = np.array([
	mc.cost([gamma], jumps_V6, included_clusters) 
	for gamma in gamma_range])
fig_cost, ax_cost = plt.subplots()
fig_cost, ax_cost, lines_cost = mc.plot_cost(
	gamma_range, cost, gamma_min, cost_min)
plt.show()
\end{lstlisting}
\begin{figure}
\centering
\includegraphics[width=\linewidth]{cost_plot.png}
\caption{Plot of the cost function, for rat V6, defined in Eq. \ref{eq:cost}. The red circle marks the minimum found by the function minimize\_cost.}
\label{fig:cost_plot}
\end{figure}

The frequency jump points can now be obtained with the optimal value of gamma. The function \lstinline{poly_cluster} inputs an array of jump points and a dictionary of vertices and outputs an array of integers called cluster. As mentioned before, the integers of cluster each represent a distinct mode transition. The global variable LEGEND is a lookup table that gives the correspondence between the entry of cluster and the mode transition. Clustering the jump points, using our minimizing value of $\gamma$, a result, somewhat better than out initial guess, is obtained (Fig. \ref{fig:clustered_jumps}).
\begin{lstlisting}[language=Python]
slopes = pc.calc_slopes(gamma_min, included_clusters)
bisecting_slopes = pc.find_bisector(slopes)
vertices = pc.polygon_vertices(bisecting_slopes, 
	included_clusters)
cluster = pc.poly_cluster(jumps_V6, vertices)
fig_jumps, ax_jumps, line_jumps = (
	pc.plot_jump_frequencies(jumps_V6, cluster))
\end{lstlisting}
\begin{figure}
\centering
\includegraphics[width=\linewidth]{clustered_jumps.png}
\caption{Jump points, for the rat V6, clustered using the optimal value of $\gamma$. The color coding of the points indicates their assigned cluster.}
\label{fig:clustered_jumps}
\end{figure}

The value of this result is two-fold. First, it gives an estimation for the parameter $\gamma$. This gives an indication of the mechanism which is being used to produce the vocalization. This and how to obtain confidence intervals for these estimates is discussed more in a later section. Second, it gives a method for classifying ultrasonic vocalizations. Further exploration of the data shows that if vocalizations, with jump points in the same mode transition cluster, are compared it can be seen they are qualitatively similar. This is believed to be indicative of the fact that rats used different resonance modes to communicate different types of information. Although the actual informational content of rat ultrasonic vocalizations is largely unknown there is research to support this. As discussed in a previous chapter, it has been shown that vocalizations in the 22 kHz range are used as a warning call to other rats when a predator is detected \cite{Blanchard1991, Panksepp2007, Brudzynski1990, Burgdorf2010}. It is also been shown that calls in the 50-85 khZ range are associated with positive mental states and are used in playful and sexual situations \cite{Burgdorf2010}. The program point\_browser will be discussed more in the next section, but it can be used to interactively view the entire vocalization from which a jump point is extracted.

\subsection{Interactive Exploration of the Data}
The program point\_browser.py can be used to interactively explore the data. To open it simply run it from the terminal (with the virtualenv activated).
\begin{lstlisting}
python point_browser.py
\end{lstlisting}
The instructions for use are printed in the terminal, but are also repeated here.
\begin{itemize}
\item[] Instructions:
\item Press the up or down key to switch to data from different rats
\item Select a jump point with the mouse to view the spectrogram of the full recording.
\item You can also press n to select the next jump point and p to select the previous one.
\item Use the mode transition selector on the left to choose clusters you want included in the algorithm.
\item Press ctrl+shift+c to run the clustering algorithm. It will take less than a minute.
\item Press c to toggle plotting the fitted curve on top of the spectrogram.
\item Press a to play a downsampled version of the selected recording through your speakers.
\end{itemize}

\subsection{Inferring the Mechanism Behind Rat Ultrasonic Vocalizations}
By computing confidence intervals on the estimation of $\gamma$ for different rats information can be inferred about the mechanism by which the recorded vocalizations are produced. The bootstrapping method can be used to calculate these confidence intervals. The script bootstrap.py contains the functions necessary to implement the method. Bootstrapping works by taking measurements from an experiment and randomly sampling those measurements with replacement to get a resampled set of measurements. The number of resampled measurements is equal to the original number of measurements, but some of them may be duplicates since sampling was done with replacement. Our measurements are the extracted frequency jumps. The function resample performs the resampling procedure. The optimal value of can then be computed $\gamma$ for the resampled measurements and get a slightly different estimate of $\gamma$ than for the true set of measurements. The following code performs the resampling procedure on the array of jump frequencies and computes an estimate of $\gamma$ from the resampled measurements. It can be seen that the resampled estimate is slightly different from the estimate obtained from the original data.
\begin{lstlisting}[language=Python]
jumps_resampled = bootstrap.resample(jumps_V6)
gamma_estimate = mc.minimize_cost(jumps_resampled, 
	initial=-0.5, 
	included_clusters=included_clusters)
print(gamma_estimate)
>>>-0.59491155336735457
\end{lstlisting}
The idea is then to repeat this resampling and estimating procedure a large number of times to get a histogram of estimates for gamma. The following code performs the resampling and estimating procedure on the jump frequency measurements 1000 times.
\begin{lstlisting}[language=Python]
resampling_generator = ( 
	bootstrap.resample(jumps_V6) 
	for n in range(1000) )
gamma_estimates = ([
	mc.minimize_cost(jumps_resampled, 
		-0.5, included_clusters) 
	for jumps_resampled in resampling_generator])
\end{lstlisting}
This is his however very computationally intensive to run so resampled estimates for $\gamma$ are included in this repository. Loading the precomputed estimates and computing their histogram it can be seen they can be interpreted as a probability distribution for the true value of $\gamma$ (Fig. \ref{fig:gamma_histogram}).
\begin{lstlisting}[language=Python]
gamma_estimates = np.load(
  './gamma_estimates/V6_estimates.npy')[:,0]
fig_hist, ax_hist = plt.subplots()
num_bins = int(np.sqrt(len(gamma_estimates)))
weights = (
  np.ones_like(gamma_estimates)/len(gamma_estimates))
ax_hist.hist(gamma_estimates, num_bins, weights=weights)
ax_hist.set_xlabel('gamma')
ax_hist.set_ylabel('probability')
ax_hist.set_title('Histogram of Gamma Estimates')
plt.show()
\end{lstlisting}
\begin{figure}
\centering
\includegraphics[width=\linewidth]{gamma_histogram.png}
\caption{Histogram of 1000 resampled estimates of $\gamma$ for rat V6.}
\label{fig:gamma_histogram}
\end{figure}

With the interpretation of the entries of gamma\_estimates as being drawn from this probability distribution confidence intervals can be computed for the estimation of $\gamma$. To compute the 95\% confidence interval of this estimate the function calc\_ci is used with the key word argument confidence=0.025. The function returns the endpoints of the confidence interval. The function plot\_confidence calculates and plots confidence intervals on $\gamma$ for all of the rats and compares them to known values of $\gamma$ for different acoustic systems.
\begin{lstlisting}[language=Python]
a, b = bootstrap.calc_ci(gamma_estimates, 
  confidence=0.025)
bootstrap.plot_confidence()
\end{lstlisting}
\begin{figure}
\centering
\includegraphics[width=\linewidth]{confidence_intervals.png}
\caption{Estimates of $\gamma$ for different rats along with $95$ \% confidence intervals computed using the bootstrapping method (blue). Also shown are known values of $\gamma$ for different aerodynamic whistles. It can be seen that the value of $\gamma$ for the hole tone falls well outside all computed confidence intervals. Therefore, it is very unlikely that this mechanism responsible for rodent USVs.}
\label{fig:confidence_intervals}
\end{figure}

In the plot in Fig. \ref{fig:confidence_intervals} it can be seen that the values of $\gamma$ for the different rats seem to group around -0.6. Some rats give a slightly different value for $\gamma$. This is probably because these rats did not exhibit vocalizations in mode regimes seen in other rats. The rat V1 for example only seemed to vocalize in the n=3 and n=4 modes. This may have affected the estimation of $\gamma$ for this rat. Nevertheless the value of $\gamma$ for the hole tone is well outside the confidence intervals for $\gamma$ of the different rats. Thus, it is very unlikely that this is the source of their vocalizations. Also shown in this plot are known values of $\gamma$ for different acoustic mechanisms. Based on this plot, the edge tone is a candidate for the true mechanism. However, the anatomy of the rat vocal tract makes this an unlikely source. The half open pipe is anatomically consistent with the rat vocal tract, but it still lies outside most of the calculated confidence intervals. Further theoretical work needs to be done on modeling the acoustics of the rat vocal tract. It is my view that the true mechanism is a combination of the hole tone and the half open pipe, where vorticity growth of an axisymmetric jet drives the resonance modes of a half open pipe.