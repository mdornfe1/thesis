\contentsline {chapter}{\numberline {1}Background on Sonic and Ultrasonic Vocalizations}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Ethology of Rodent Ultrasonic Vocalizations}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Anatomy of the Larynx}{8}{section.1.3}
\contentsline {section}{\numberline {1.4}Physics of Vibrating Vocal Folds}{12}{section.1.4}
\contentsline {section}{\numberline {1.5}The Failure of Vibrating Vocal Fold Models for Ultrasonic Vocalizations}{19}{section.1.5}
\contentsline {chapter}{\numberline {2}Fluid Mechanics and Acoustics Background}{29}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{29}{section.2.1}
\contentsline {section}{\numberline {2.2}Conservation Laws}{30}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Mass Conservation}{30}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Energy Conservation}{31}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Momentum Conservation}{32}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}The Stress Tensor}{35}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Symmetry of the Stress Tensor}{36}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}The Stress Tensor in a Static Fluid}{38}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}The Stress Tensor in a Moving Fluid}{40}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}The Navier-Stokes and Euler Equations}{41}{section.2.4}
\contentsline {section}{\numberline {2.5}Linear Acoustic Flow}{43}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Introduction}{43}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}The Acoustic Wave Equation}{44}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}Acoustic Boundary Value Problems}{46}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Formulation of the Boundary Value Problem}{46}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Impedance Boundary Conditions}{49}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}Calculation of the Mouth Impedance}{50}{subsection.2.6.3}
\contentsline {subsection}{\numberline {2.6.4}An Example}{53}{subsection.2.6.4}
\contentsline {section}{\numberline {2.7}Inviscid, Incompressible, Potential Flow}{59}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Bernoulli's Equation}{59}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}The Continuity Equation}{61}{subsection.2.7.2}
\contentsline {section}{\numberline {2.8}Nonlinear Energy Losses at an Orifice}{63}{section.2.8}
\contentsline {chapter}{\numberline {3}Data Analysis of Rodent Ultrasonic Vocalizations}{66}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{66}{section.3.1}
\contentsline {section}{\numberline {3.2}Aerodynamic Whistles}{67}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Cavity Tones}{69}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}The Edge Tone}{70}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}The Hole Tone}{71}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Frequency Jumps}{75}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Data Collection}{76}{section.3.3}
\contentsline {section}{\numberline {3.4}Analysis and Discussion}{77}{section.3.4}
\contentsline {chapter}{\numberline {4}Time Domain Model of the Rodent Vocal Tract}{91}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{91}{section.4.1}
\contentsline {section}{\numberline {4.2}Time Domain Models of Organ Pipes}{93}{section.4.2}
\contentsline {section}{\numberline {4.3}Derivation of the Model}{99}{section.4.3}
\contentsline {section}{\numberline {4.4}The Spatial Eigenmodes}{103}{section.4.4}
\contentsline {section}{\numberline {4.5}The Viscous Damping Approximation}{108}{section.4.5}
\contentsline {section}{\numberline {4.6}The Flow Through the Vocal Folds}{110}{section.4.6}
\contentsline {section}{\numberline {4.7}The Time Domain ODEs}{116}{section.4.7}
\contentsline {chapter}{\numberline {5}The Force Due to Vorticity}{122}{chapter.5}
\contentsline {section}{\numberline {5.1}Introduction}{122}{section.5.1}
\contentsline {section}{\numberline {5.2}The Force Due to Unsteady Vorticity}{123}{section.5.2}
\contentsline {section}{\numberline {5.3}Linear Vorticity Evolution in a Pipe}{125}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}The Linearized Flow Equations}{126}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}The Chebyshev Method}{129}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}The Temporal or Initial Value Problem}{130}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}The Spatial or Signaling Problem}{140}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}Discussion}{148}{subsection.5.3.5}
\contentsline {section}{\numberline {5.4}Nonlinear Time Domain Approach}{149}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Connection to Acoustic Flow}{154}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Analysis and Discussion}{158}{subsection.5.4.2}
\contentsline {chapter}{Bibliography}{164}{section*.50}
