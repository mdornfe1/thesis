\begin{thebibliography}{10}

\bibitem{Gray2000Anatomy}
H. Gray, W.~H. Lewis, and Bartleby, {Anatomy of the human body}, 2000.

\bibitem{Titze2006}
I.~R. Titze, {\em The Myoelastic Aerodynamic Theory of Phonation} (National
  Center for Voice and Speech, Salt Lake City, 2006).

\bibitem{Gardner2001}
T. Gardner {\it et~al.}, Physical Review Letters {\bf 87},    (2001).

\bibitem{Fletcher2010}
N.~H. Fletcher, Handbook of Behavioral Neuroscience {\bf 19},  51  (2010).

\bibitem{white1998}
N.~R. White, M. Prasad, R.~J. Barfield, and J.~G. Nyby, Physiology \& Behavior
  {\bf 63},  467  (1998).

\bibitem{berry1970natural}
R. Berry, Field Studies {\bf 3},  219  (1970).

\bibitem{Fenton1998}
M.~B. Fenton, C.~V. Portfors, I.~L. Rautenbach, and J.~M. Waterman, Canadian
  Journal of Zoology {\bf 76},  1174  (1998).

\bibitem{Jones2006}
G. Jones and E. Teeling, Trends in Ecology \& Evolution {\bf 21},  149  (2006).

\bibitem{bogdanowicz1994}
W. Bogdanowicz, Mammalian species  1  (1994).

\bibitem{Frankel2009}
A.~S. Frankel, Encyclopedia of Marine Mammals  1056  (2009).

\bibitem{Whitehead2009}
H. Whitehead, Encyclopedia of Marine Mammals  1091  (2009).

\bibitem{Rendell1999}
L.~E. Rendell {\it et~al.}, J Zoology {\bf 249},  403  (1999).

\bibitem{Kastelein2000}
R. Kastelein, J. Mosterd, N. Schooneman, and P. Wiepkema, Aquatic Mammals {\bf
  26},  33  (2000).

\bibitem{Jefferson1993}
T.~A. Jefferson, S. Leatherwood, and M.~A. Webber, {\em {Marine mammals of the
  world}} (Food \& Agriculture Org., Rome, 1993).

\bibitem{Sanders2001}
I. Sanders {\it et~al.}, {\em Society for Neuroscience} (Society for
  Neuroscience, Washington, DC, 2001).

\bibitem{Chanaud1970}
R.~C. Chanaud, Scientific American {\bf 222},  40  (1970).

\bibitem{Auvray2012}
R. Auvray, B. Fabre, and P.-Y. Lagr{\'e}e, The Journal of the Acoustical
  Society of America {\bf 131},  1574  (2012).

\bibitem{Titze1988a}
I.~R. Titze, J. Acoust. Soc. Am. {\bf 83},  1536  (1988).

\bibitem{Jedrzejewski1993}
W. Jedrzejewski and B. Jedrzejewska, Ecography {\bf 16},  47  (1993).

\bibitem{Brudzynski2010}
S.~M. Brudzynski and N.~H. Fletcher, Handbook of Behavioral Neuroscience {\bf
  19},  69  (2010).

\bibitem{Bass2008}
A.~H. Bass, E.~H. Gilland, and R. Baker, Science {\bf 321},  417  (2008).

\bibitem{Blumberg1990}
M.~S. Blumberg and J.~R. Alberts, Behavioral Neuroscience {\bf 104},  808
  (1990).

\bibitem{Hofer1992}
M.~A. Hofer and H.~N. Shair, Developmental Psychobiology {\bf 25},  511
  (1992).

\bibitem{Hofer1993}
M.~A. Hofer and H.~N. Shair, Behavioral Neuroscience {\bf 107},  354  (1993).

\bibitem{Hofer2010}
M.~A. Hofer, Handbook of Mammalian Vocalization - An Integrative Neuroscience
  Approach  29  (2010).

\bibitem{Brudzynski2007a}
S.~M. Brudzynski, Behavioural brain research {\bf 182},  261  (2007).

\bibitem{Blanchard1991}
R. Blanchard, D. Blanchard, R. Agullana, and S.~M. Weiss, Physiology \&
  Behavior {\bf 50},  967  (1991).

\bibitem{Panksepp2007}
J. Panksepp {\it et~al.}, Behavioral Neuroscience {\bf 121},  1364  (2007).

\bibitem{Brudzynski1990}
S.~M. Brudzynski and F. Bihari, Neuroscience Letters {\bf 109},  222  (1990).

\bibitem{Burgdorf2010}
J. Burgdorf and J.~R. Moskal, Handbook of Mammalian Vocalization - An
  Integrative Neuroscience Approach  209  (2010).

\bibitem{Berke2010}
G.~S. Berke and J.~L. Long, Handbook of Behavioral Neuroscience {\bf 19},  419
  (2010).

\bibitem{Damrose2003}
E.~J. Damrose {\it et~al.}, Annals of Otology, Rhinology \& Laryngology {\bf
  112},  434  (2003).

\bibitem{Titze2000}
I. Titze, {\em {Principles of voice production}} (National Center for Voice and
  Speech, Salt Lake City, 2000).

\bibitem{Choi1993}
H.-S. Choi, M. Ye, G.~S. Berke, and J. Kreiman, Annals of Otology, Rhinology \&
  Laryngology {\bf 102},  769  (1993).

\bibitem{Titze1988}
I.~R. Titze, J. Jiang, and D.~G. Drucker, Journal of Voice {\bf 1},  314
  (1988).

\bibitem{bradbury1998principles}
J.~W. Bradbury and S.~L. Vehrencamp, {\em {Principles of animal communication}}
  (Sinauer Associates, Sunderland, MA, 1998).

\bibitem{Roberts1975}
L. Roberts, Ultrasonics {\bf 13},  83  (1975).

\bibitem{Riede2011}
T. Riede, Journal of Neurophysiology {\bf 106},  2580  (2011).

\bibitem{Riede2011c}
T. Riede {\it et~al.}, Journal of Biomechanics {\bf 44},  1936  (2011).

\bibitem{Inagi1998a}
K. Inagi, E. Schultz, and C.~N. Ford, Otolaryngology -- Head and Neck Surgery
  {\bf 118},  74  (1998).

\bibitem{Batchelor2000}
G.~K. Batchelor, {\em {An Introduction to Fluid Dynamics}} (Cambridge
  University Press, Cambridge, 2000), cambridge Books Online.

\bibitem{howe2003theory}
M.~S. Howe, {\em {Theory of vortex sound}} (Cambridge University Press,
  Cambridge, 2003), Vol.~33.

\bibitem{Chanaud1965}
R.~C. Chanaud and A. Powell, The Journal of the Acoustical Society of America
  {\bf 37},  902  (1965).

\bibitem{fletcher2012physics}
N.~H. Fletcher and T. Rossing, {\em {The physics of musical instruments}}
  (Springer Science \& Business Media, New York, 2012).

\bibitem{rayleigh1896theory}
J.~W. S.~B. Rayleigh, {\em {The theory of sound}} (Macmillan, New York, 1896),
  Vol.~2.

\bibitem{pierce1981acoustics}
A.~D. Pierce {\it et~al.}, {\em {Acoustics: an introduction to its physical
  principles and applications}} (McGraw-Hill, New York, 1981), Vol.~20.

\bibitem{Aarts2003}
R.~M. Aarts and A.~J. E.~M. Janssen, J. Acoust. Soc. Am. {\bf 113},  2635
  (2003).

\bibitem{rienstra2003introduction}
S.~W. Rienstra and A. Hirschberg, Eindhoven University of Technology {\bf 18},
  19  (2003).

\bibitem{Cummings1986}
A. Cummings and I.-J. Chang, Revue de Physique Appliquée {\bf 21},  151–161
  (1986).

\bibitem{Ingard1967}
U. Ingard, J. Acoust. Soc. Am. {\bf 42},  6  (1967).

\bibitem{Howe2008}
M.~S. Howe, {\em {Acoustics of Fluid-Structure Interactions (Cambridge
  Monographs on Mechanics)}} (Cambridge University Press, Cambridge, 2008).

\bibitem{rossiter1962}
J. Rossiter, Technical report, Royal Aircraft Establishment, RAE Farnborough
  (unpublished).

\bibitem{holger1977}
D.~K. Holger, T.~A. Wilson, and G.~S. Beavers, The Journal of the Acoustical
  Society of America {\bf 62},  1116  (1977).

\bibitem{coltman1976}
J.~W. Coltman, The Journal of the Acoustical Society of America {\bf 60},  725
  (1976).

\bibitem{Fletcher1976}
N.~H. Fletcher, Acta Acustica united with Acustica {\bf 34},  224  (1976).

\bibitem{Auvray2012a}
R. Auvray, B. Fabre, and P.-Y. Lagrée, J. Acoust. Soc. Am. {\bf 131},  1574
  (2012).

\bibitem{Henwood2002}
D. Henwood, Journal of Sound and Vibration {\bf 254},  575–593  (2002).

\bibitem{akhmetov2009vortex}
D.~G. Akhmetov, {\em Vortex rings} (Springer Science \& Business Media, New
  York, 2009).

\bibitem{Morris1976}
P.~J. Morris, Journal of Fluid Mechanics {\bf 77},  511  (1976).

\bibitem{trefethen2000spectral}
L.~N. Trefethen, {\em Spectral methods in MATLAB} (Siam, Philadelphia, 2000),
  Vol.~10.

\bibitem{Batchelor1962}
G.~K. Batchelor and A.~E. Gill, J. Fluid Mech. {\bf 14},  529  (1962).

\bibitem{Horwitz1992}
J. Horwitz and S. Rosenblat, Acta Mechanica {\bf 95},  131–156  (1992).

\bibitem{Arneodo2009}
E.~M. Arneodo and G.~B. Mindlin, Phys. Rev. E {\bf 79},    (2009).

\end{thebibliography}
